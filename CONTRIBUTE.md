# Contributing

Any help is welcome.

1. Install cargo-gra 
    ```
    cargo install cargo-gra --locked
    ```
2. Run
    ```
    cargo gra gen
    ```
3. Build flatpak
    ```
    cargo gra flatpak
    ```
   or build binary
    ```
    cargo build --release
    ```


## Required dependencies

```
sudo apt install libgtk-4-dev
# and libadwaita which is not available yet :(
```
