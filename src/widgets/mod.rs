mod dumb;
mod smart;

pub use dumb::*;
pub use smart::*;

pub fn init() {
    use glib::StaticType;
    DatetimeChooser::static_type();
    RepetitionRow::static_type();

    MonthGrid::static_type();
    MonthHeaderCell::static_type();
    MonthCell::static_type();

    EventTag::static_type();
    AddRow::static_type();
    RemoveRow::static_type();
}
