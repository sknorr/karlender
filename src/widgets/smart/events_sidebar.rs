use crate::domain::Event;
use crate::store::gstore::*;
use crate::store::{store, State};
use crate::widgets::EventRow;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "events_sidebar.ui")]
pub struct EventsSidebar {
    #[template_child]
    pub event_box: TemplateChild<gtk::Box>,
}
impl EventsSidebar {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create EventsSidebar")
    }
    pub fn constructed(&self) {
        let s = self;

        select!(|state|
                state.calendar_slice,
                state.selection_slice,
                state.settings,
            => glib::clone!(@weak s => move |state| {
                s.on_state_update(state)
            })
        );
    }

    fn on_state_update(&self, state: &State) {
        if let Some(selection) = state
            .selection_slice
            .selection
            .as_ref()
            .or(state.selection_slice.former_selection.as_ref())
        {
            let dateformat = &state.settings.dateformat;
            let timezone = state.settings.timezone();
            let event_box = &self.imp().event_box;

            let mut events = Vec::new();
            for (cid, calendar) in &state.calendar_slice.calendars {
                for event in calendar.events.values() {
                    if event.contains_date(&selection.start.date()) {
                        events.push((cid, event))
                    }
                }
            }
            while let Some(child) = event_box.first_child() {
                event_box.remove(&child);
            }
            events.sort_by(|(_, a), (_, b)| Event::sort(b, a));
            for (calendar_id, event) in &events {
                let event_row = EventRow::new(
                    dateformat.clone(),
                    timezone.clone(),
                    calendar_id,
                    event,
                    true,
                );
                event_row.connect_remove(|r| {
                    dispatch!(crate::store::REMOVE_EVENT, (r.calendar_id(), r.event_id()));
                });
                event_row.connect_select(|r| {
                    dispatch!(crate::store::EDIT_EVENT, (r.calendar_id(), r.event_id()));
                    dispatch!(crate::store::NAVIGATE, crate::pages::EDITOR_PAGE_NAME);
                });
                event_box.prepend(&event_row);
            }
        }
    }
}

impl Default for EventsSidebar {
    fn default() -> Self {
        Self::new()
    }
}
