use crate::domain::*;
use crate::store::{gstore::*, store, State};
use crate::widgets::EventTag;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use glib::subclass::signal::Signal;
use gtk::{prelude::*, GestureClick};
use libadwaita as adw;
use std::cell::Cell;

#[widget(extends gtk::Box)]
#[template(file = "month_cell.ui")]
struct MonthCell {
    pub date: Cell<Option<KarlenderDate>>,

    #[template_child]
    pub cell_leaflet: TemplateChild<adw::Leaflet>,
    #[template_child]
    pub day_label: TemplateChild<gtk::Label>,
    #[template_child]
    pub event_leaflet: TemplateChild<adw::Leaflet>,

    #[property_bool]
    pub current_month: Cell<bool>,

    #[signal(Signal::builder("clicked", &[i32::static_type().into()], <()>::static_type().into()).build())]
    clicked: (),
}

fn on_state_update(s: MonthCell, state: &State, date: KarlenderDate) {
    let event_leaflet = &s.imp().event_leaflet;
    let calendars = &state.calendar_slice.calendars;

    let mut child = event_leaflet.first_child();
    while child.is_some() {
        let c = child.unwrap();
        let next = c.next_sibling();
        event_leaflet.remove(&c);
        child = next;
    }

    let mut events = Vec::new();
    for calendar in calendars.values() {
        if !calendar.enabled {
            continue;
        }
        for event in calendar.events.values() {
            if event.contains_date(&date) {
                events.push((event, calendar.color))
            }
        }
    }
    events.sort_by(|(a, _), (b, _)| Event::sort(a, b));

    let events_len = events.len() as u64;
    for (event, color) in events {
        let tag = EventTag::new(event.name.clone(), events_len, color);
        event_leaflet.connect_folded_notify(glib::clone!(@weak tag => move |l| {
            let placeholder_label = tag.last_child().unwrap();
            let placeholder_num = placeholder_label.prev_sibling().unwrap();
            let tag_label = placeholder_num.prev_sibling().unwrap();
            if l.is_folded() {
                placeholder_label.set_visible(true);
                placeholder_num.set_visible(true);
                tag_label.set_visible(false);
            } else {
                placeholder_label.set_visible(false);
                placeholder_num.set_visible(false);
                tag_label.set_visible(true);
            }
        }));
        event_leaflet.append(&tag);
    }
}

impl MonthCell {
    pub fn new(date: KarlenderDate, current_month: bool) -> Self {
        let month_cell: Self = glib::Object::new(&[("current-month", &current_month)])
            .expect("Failed to create MonthGrid");
        month_cell.imp().date.set(Some(date));
        month_cell
    }

    pub fn constructed(&self) {
        let s = self;

        self.connect_realize(|s| s.realized());

        let gesture_click = GestureClick::builder().build();
        gesture_click.connect_released(glib::clone!(@weak s => move |_, n_press, _, _| {
            s.emit_by_name("clicked", &[&n_press.to_value()])
        }));

        let events_leaflet: &adw::Leaflet = self.imp().event_leaflet.as_ref();
        self.imp().cell_leaflet.set_visible_child(events_leaflet);
        self.add_controller(&gesture_click);
    }

    pub fn realized(&self) {
        let d = self.imp().date.take();
        let date = d.clone();
        self.imp().date.set(d);

        if let Some(date) = date {
            self.imp().day_label.set_label(&format!("{}", date.day()));
            self.set_styles();

            let s = self;
            select!(|state| state.calendar_slice
                => glib::clone!(@weak s
                    => move |state| on_state_update(s, state, date.clone())
                )
            );
        }
    }

    pub fn set_styles(&self) {
        let date = self.imp().date.take();
        let d = date.clone();
        self.imp().date.set(d);
        let date = date.unwrap();

        let current_month: bool = self.property("current-month");
        if !current_month {
            self.style_context().add_class("cell-other");
            let last = get_month_len(&date);
            if date.day() == 1 {
                self.style_context().add_class("border-l-1");
            } else if date.day() == last {
                self.style_context().add_class("border-r-1");
            }
            if date.day() > 20 {
                self.style_context().add_class("border-b-1");
            } else {
                self.style_context().add_class("border-t-1");
            }
        } else {
            self.style_context().add_class("view");
        }

        if date == KarlenderTimezone::local().today() {
            self.style_context().add_class("cell-current");
        }
    }

    pub fn connect_clicked(&self, f: impl Fn(&Self, i32) + 'static) {
        self.connect_closure(
            "clicked",
            false,
            closure_local!(move |s: Self, num_clicks: i32| {
                f(&s, num_clicks);
            }),
        );
    }

    pub fn date(&self) -> KarlenderDate {
        let d = self.imp().date.take().unwrap();
        let date = d.clone();
        self.imp().date.set(Some(d));
        date
    }
}
