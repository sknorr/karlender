use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use glib::once_cell::sync::OnceCell;
use gtk::prelude::*;
use std::cell::Cell;

static NAMES: OnceCell<[(String, String); 7]> = OnceCell::new();

#[widget(extends gtk::Box)]
#[template(file = "month_header_cell.ui")]
pub struct MonthHeaderCell {
    #[property_u64]
    pub day: Cell<u64>,
    #[template_child]
    pub label: TemplateChild<gtk::Label>,
}

impl MonthHeaderCell {
    pub fn new(day: u64) -> Self {
        glib::Object::new(&[("day", &day)]).expect("Failed to create MonthHeaderCell")
    }
    pub fn constructed(&self) {
        self.connect_realize(|s| s.realized());
    }
    fn realized(&self) {
        let s = self;

        // select!(|state| state.ui => glib::clone!(@weak s => move |state| {
        //     let ui = &state.ui;
        let day = s.imp().day.clone().take();
        let names = NAMES.get_or_init(|| {
            [
                (gettext("Monday"), gettext("Mo")),
                (gettext("Tuesday"), gettext("Tu")),
                (gettext("Wednesday"), gettext("We")),
                (gettext("Thursday"), gettext("Th")),
                (gettext("Friday"), gettext("Fr")),
                (gettext("Saturday"), gettext("Sa")),
                (gettext("Sunday"), gettext("Su")),
            ]
        });
        //     if ui.mobile {
        s.imp().label.set_label(&names[day as usize].1)
        //     } else {
        // s.imp().label.set_label(&names[day as usize].0)
        //     }
        // }));
    }
}
