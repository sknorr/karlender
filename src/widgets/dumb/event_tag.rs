use gdk4::subclass::prelude::ObjectSubclassIsExt;
use std::cell::Cell;

use crate::domain::KarlenderColor;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "event_tag.ui")]
pub struct EventTag {
    #[property_string]
    pub label: Cell<String>,
    #[property_u64]
    pub num: Cell<u64>,

    #[template_child]
    pub event_tag_label: TemplateChild<gtk::Label>,
    #[template_child]
    pub events_placeholder_num: TemplateChild<gtk::Label>,
    #[template_child]
    pub events_placeholder_label: TemplateChild<gtk::Label>,
}

impl EventTag {
    pub fn constructed(&self) {}

    pub fn new(label: String, num: u64, color: KarlenderColor) -> Self {
        let s: Self = glib::Object::new(&[("label", &label), ("num", &num)])
            .expect("Failed to create EventTag");
        s.add_css_class(color.style_class());
        s
    }
}
