use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
use gtk::{prelude::*, subclass::list_box_row::ListBoxRowImpl};
use libadwaita as adw;

#[widget(extends adw::ActionRow)]
#[template(file = "add_row.ui")]
pub struct AddRow {}

impl PreferencesRowImpl for imp::AddRow {}

impl ListBoxRowImpl for imp::AddRow {}

impl AddRow {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create AddRow")
    }
    pub fn constructed(&self) {
        let inner_box = self.first_child().unwrap();
        inner_box.set_halign(gtk::Align::Center);
    }
}

impl Default for AddRow {
    fn default() -> Self {
        Self::new()
    }
}
