use crate::domain::*;

use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
use gdk4::subclass::prelude::ObjectSubclassIsExt;

use gtk::prelude::*;
use gtk::subclass::list_box_row::ListBoxRowImpl;
use libadwaita as adw;
use std::cell::Cell;

#[widget(extends adw::ActionRow)]
#[template(file = "account_row.ui")]
pub struct AccountRow {
    pub login: Cell<Login>,
    #[callback]
    on_remove: (),
    #[signal]
    removed: (),
}

impl AccountRow {
    pub fn new(login: &Login) -> Self {
        let s: Self = glib::Object::new(&[
            //
            ("title", &login.login),
            ("subtitle", &login.url),
        ])
        .expect("Failed to create AccountRow");
        s.imp().login.set(login.clone());
        s
    }

    pub fn constructed(&self) {}

    fn on_remove(&self, _: gtk::Button) {
        self.emit_removed();
    }

    pub fn connect_removed(&self, f: impl Fn(&Self, Login) + 'static) {
        self._connect_removed(move |s| f(s, s.imp().login.take()));
    }
}

impl PreferencesRowImpl for imp::AccountRow {}

impl ListBoxRowImpl for imp::AccountRow {}
