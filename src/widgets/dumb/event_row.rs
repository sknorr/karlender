use std::cell::Cell;

use crate::domain::*;
use crate::store::settings::DateFormats;
use adw::prelude::{ActionRowExt, PreferencesRowExt};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use glib::SignalHandlerId;
use gtk::prelude::*;
use libadwaita as adw;

#[widget(extends gtk::Box)]
#[template(file = "event_row.ui")]
pub struct EventRow {
    #[property_string]
    pub calendar_id: Cell<String>,
    #[property_string]
    pub event_id: Cell<String>,
    #[property_bool]
    pub small: Cell<bool>,

    pub dateformat: Cell<Option<DateFormats>>,
    pub timezone: Cell<Option<KarlenderTimezone>>,
    pub event: Cell<Option<Event>>,

    #[template_child]
    pub row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub remove_button: TemplateChild<gtk::Button>,

    #[signal]
    remove: (),
    #[signal]
    select: (),

    #[callback]
    on_remove: (),
    #[callback]
    on_select: (),
}
impl EventRow {
    pub fn new(
        dateformat: Option<DateFormats>,
        timezone: Option<KarlenderTimezone>,
        calendar_id: &str,
        event: &Event,
        small: bool,
    ) -> Self {
        let s: Self = glib::Object::new(&[
            ("calendar-id", &calendar_id),
            ("event-id", &event.id),
            ("small", &small),
        ])
        .expect("Failed to create EventRow");
        s.imp().dateformat.set(dateformat);
        s.imp().timezone.set(timezone);
        s.imp().event.set(Some(event.clone()));
        s
    }

    pub fn constructed(&self) {
        self.connect_realize(|s| s.realized());
    }

    fn realized(&self) {
        let row = &self.imp().row;
        let small: bool = self.property("small");

        let tz = self.imp().timezone.take();
        let timezone = tz.clone();
        self.imp().timezone.set(tz);

        let dateformat = self.imp().dateformat.take();
        self.imp().dateformat.set(dateformat.clone());

        let timezone = timezone.unwrap();

        if let Some(event) = self.imp().event.take() {
            row.set_title(&event.name);
            if let Some(df) = dateformat {
                if event.full_day {
                    let start = timezone.from_utc(&event.start);
                    let end = timezone
                        .from_utc(&(event.end.clone() - KarlenderDateTimeDuration::days(1)));
                    if small {
                        if start == end {
                            row.set_subtitle(&format!(
                                "{}{}",
                                start.format(df.date),
                                event
                                    .location
                                    .as_ref()
                                    .map(|l| format!("\n{}", l))
                                    .unwrap_or_else(|| "".into())
                            ));
                        } else {
                            row.set_subtitle(&format!(
                                "{}\n{}{}",
                                start.format(df.date),
                                end.format(df.date),
                                event
                                    .location
                                    .as_ref()
                                    .map(|l| format!("\n{}", l))
                                    .unwrap_or_else(|| "".into())
                            ));
                        }
                    } else if start == end {
                        row.set_subtitle(&format!(
                            "{}{}",
                            start.format(df.date),
                            event
                                .location
                                .as_ref()
                                .map(|l| format!("\n{}", l))
                                .unwrap_or_else(|| "".into())
                        ));
                    } else {
                        row.set_subtitle(&format!(
                            "{} - {}{}",
                            start.format(df.date),
                            end.format(df.date),
                            event
                                .location
                                .as_ref()
                                .map(|l| format!("\n{}", l))
                                .unwrap_or_else(|| "".into())
                        ));
                    }
                } else {
                    let start = timezone.from_utc(&event.start);
                    let end = timezone.from_utc(&event.end);
                    if small {
                        if start.date() == end.date() {
                            row.set_subtitle(&format!(
                                "{}\n{} - {}{}",
                                start.format(df.date),
                                start.format(df.time),
                                end.format(df.time),
                                event
                                    .location
                                    .as_ref()
                                    .map(|l| format!("\n{}", l))
                                    .unwrap_or_else(|| "".into())
                            ));
                        } else {
                            row.set_subtitle(&format!(
                                "{}\n{}{}",
                                start.format(df.datetime),
                                end.format(df.datetime),
                                event
                                    .location
                                    .as_ref()
                                    .map(|l| format!("\n{}", l))
                                    .unwrap_or_else(|| "".into())
                            ));
                        }
                    } else if start.date() == end.date() {
                        row.set_subtitle(&format!(
                            "{} {} - {}{}",
                            start.format(df.date),
                            start.format(df.time),
                            end.format(df.time),
                            event
                                .location
                                .as_ref()
                                .map(|l| format!("\n{}", l))
                                .unwrap_or_else(|| "".into())
                        ));
                    } else {
                        row.set_subtitle(&format!(
                            "{} - {}{}",
                            start.format(df.datetime),
                            end.format(df.datetime),
                            event
                                .location
                                .as_ref()
                                .map(|l| format!("\n{}", l))
                                .unwrap_or_else(|| "".into())
                        ));
                    }
                }
            }
        }
    }

    fn on_select(&self, _: adw::ActionRow) {
        self.emit_select();
    }

    fn on_remove(&self, _: gtk::Button) {
        self.emit_remove();
    }

    pub fn calendar_id(&self) -> String {
        self.property("calendar-id")
    }

    pub fn event_id(&self) -> String {
        self.property("event-id")
    }

    pub fn connect_remove(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_remove(f)
    }
    pub fn connect_select(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_select(f)
    }
}
