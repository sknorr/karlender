use crate::domain::{DatetimeFormat, KarlenderDate, KarlenderDateTime, KarlenderTimezone};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use gtk::prelude::*;
use std::cell::Cell;

#[widget(extends gtk::Box)]
#[template(file = "datetime_chooser.ui")]
pub struct DatetimeChooser {
    #[property_string]
    pub date: Cell<String>,

    #[property_bool]
    pub full_day: Cell<bool>,

    #[template_child]
    pub day_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub month_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub year_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub hour_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub minute_spin_button: TemplateChild<gtk::SpinButton>,

    #[template_child]
    pub day_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub month_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub year_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub hour_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub minute_adjustment: TemplateChild<gtk::Adjustment>,

    pub timezone: Cell<Option<KarlenderTimezone>>,
    pub dateformat: Cell<Option<DatetimeFormat>>,

    #[signal]
    changed: (),
}

impl DatetimeChooser {
    pub fn constructed(&self) {
        let day_adjustment = &self.imp().day_adjustment;
        let month_adjustment = &self.imp().month_adjustment;
        let year_adjustment = &self.imp().year_adjustment;
        let hour_adjustment = &self.imp().hour_adjustment;
        let minute_adjustment = &self.imp().minute_adjustment;
        let month_spin_button = &self.imp().month_spin_button;

        month_spin_button.connect_input(|b| to_adjustment_value(b.text().to_string()));
        month_spin_button.connect_output(|b| {
            adjustment_value_to_month(b);
            gtk::Inhibit(true)
        });

        let s = self;
        day_adjustment.connect_value_changed(glib::clone!(@weak s => move |_a| {
            s.emit_changed();
        }));

        month_adjustment.connect_value_changed(glib::clone!(@weak s => move |_a| {
            s.emit_changed();
        }));

        year_adjustment.connect_value_changed(glib::clone!(@weak s => move|_a| {
            s.emit_changed();
        }));

        hour_adjustment.connect_value_changed(glib::clone!(@weak s => move|_a| {
            s.emit_changed();
        }));

        minute_adjustment.connect_value_changed(glib::clone!(@weak s => move|_a| {
            s.emit_changed();
        }));
    }

    pub fn text(&self) -> String {
        self.property("text")
    }

    pub fn connect_changed(&self, f: impl Fn(&Self) + 'static) {
        self._connect_changed(f);
    }

    /// Returns the UTC version of the selected datetime
    pub fn date(&self) -> KarlenderDateTime {
        if let Some(tz) = self.imp().timezone.take() {
            let date = KarlenderDate::from_ymd(
                self.imp().year_adjustment.value() as i32,
                self.imp().month_adjustment.value() as u8,
                self.imp().day_adjustment.value() as u8,
            )
            .and_hms(
                self.imp().hour_adjustment.value() as u8,
                self.imp().minute_adjustment.value() as u8,
                0,
                Some(&tz),
            );
            let date = tz.to_utc(&date);
            self.imp().timezone.set(Some(tz));
            date
        } else {
            warn!("DatetimeChooser returns date without timezone");
            KarlenderDate::from_ymd(
                self.imp().year_adjustment.value() as i32,
                self.imp().month_adjustment.value() as u8,
                self.imp().day_adjustment.value() as u8,
            )
            .and_hms(
                self.imp().hour_adjustment.value() as u8,
                self.imp().minute_adjustment.value() as u8,
                0,
                None,
            )
        }
    }

    pub fn set_date(
        &self,
        dateformat: DatetimeFormat,
        timezone: &Option<KarlenderTimezone>,
        date: KarlenderDateTime,
    ) {
        self.imp().dateformat.set(Some(dateformat));
        self.imp().timezone.set(timezone.clone());

        if let Some(tz) = timezone {
            let date = tz.from_utc(&date);
            self.imp().day_adjustment.set_value(date.day() as f64);
            self.imp().month_adjustment.set_value(date.month() as f64);
            self.imp().year_adjustment.set_value(date.year() as f64);
            self.imp().hour_adjustment.set_value(date.hour() as f64);
            self.imp().minute_adjustment.set_value(date.minute() as f64);
            debug!(
                "Update tz date: {}.{}.{} {}:{}",
                date.day(),
                date.month(),
                date.year(),
                date.hour(),
                date.minute()
            );
        } else {
            self.imp().day_adjustment.set_value(date.day() as f64);
            self.imp().month_adjustment.set_value(date.month() as f64);
            self.imp().year_adjustment.set_value(date.year() as f64);
            self.imp().hour_adjustment.set_value(date.hour() as f64);
            self.imp().minute_adjustment.set_value(date.minute() as f64);
            debug!(
                "Update date: {}.{}.{} {}:{}",
                date.day(),
                date.month(),
                date.year(),
                date.hour(),
                date.minute()
            );
        }
    }

    pub fn set_full_day(&self, full_day: bool) {
        self.set_property("full-day", full_day);
    }
}

fn adjustment_value_to_month(b: &gtk::SpinButton) {
    match b.adjustment().value() as u32 {
        1 => b.set_text(&gettext("Jan")),
        2 => b.set_text(&gettext("Feb")),
        3 => b.set_text(&gettext("Mar")),
        4 => b.set_text(&gettext("Apr")),
        5 => b.set_text(&gettext("May")),
        6 => b.set_text(&gettext("Jun")),
        7 => b.set_text(&gettext("Jul")),
        8 => b.set_text(&gettext("Aug")),
        9 => b.set_text(&gettext("Sep")),
        10 => b.set_text(&gettext("Oct")),
        11 => b.set_text(&gettext("Nov")),
        12 => b.set_text(&gettext("Dec")),
        _ => {}
    };
}

fn to_adjustment_value(s: String) -> Option<Result<f64, ()>> {
    if s == gettext("Jan") {
        Some(Ok(1_f64))
    } else if s == gettext("Feb") {
        Some(Ok(2_f64))
    } else if s == gettext("Mar") {
        Some(Ok(3_f64))
    } else if s == gettext("Apr") {
        Some(Ok(4_f64))
    } else if s == gettext("May") {
        Some(Ok(5_f64))
    } else if s == gettext("Jun") {
        Some(Ok(6_f64))
    } else if s == gettext("Jul") {
        Some(Ok(7_f64))
    } else if s == gettext("Aug") {
        Some(Ok(8_f64))
    } else if s == gettext("Sep") {
        Some(Ok(9_f64))
    } else if s == gettext("Oct") {
        Some(Ok(10_f64))
    } else if s == gettext("Nov") {
        Some(Ok(11_f64))
    } else if s == gettext("Dec") {
        Some(Ok(12_f64))
    } else {
        None
    }
}
