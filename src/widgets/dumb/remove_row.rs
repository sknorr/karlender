use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use gtk::{prelude::*, subclass::list_box_row::ListBoxRowImpl};
use libadwaita as adw;
use std::cell::Cell;

#[widget(extends adw::ActionRow)]
#[template(file = "remove_row.ui")]
pub struct RemoveRow {
    #[template_child]
    pub submit_button: TemplateChild<gtk::Button>,

    #[property_bool]
    pub needs_submit: Cell<bool>,

    /// Wait time for submitting in seconds.
    #[property_u64]
    pub wait_time: Cell<u64>,

    //
    #[signal]
    submit: (),

    #[callback]
    on_click_row: (),
    #[callback(noargs)]
    on_cancel: (),
    #[callback(noargs)]
    on_submit: (),
}

impl PreferencesRowImpl for imp::RemoveRow {}

impl ListBoxRowImpl for imp::RemoveRow {}

impl RemoveRow {
    pub fn new() -> Self {
        glib::Object::new(&[
            //
            ("needs_submit", &false),
            ("wait_time", &2),
        ])
        .expect("Failed to create RemoveRow")
    }
    pub fn constructed(&self) {
        //
    }
    fn on_click_row(&self, _: gtk::Widget) {
        self.set_property("title", &gettext("Are you sure?"));
        self.set_property("needs_submit", true);
        self.add_css_class("error");
    }
    fn on_cancel(&self) {
        self.set_property("needs_submit", false);
        self.set_property("title", &gettext("Remove"));
        self.remove_css_class("error");
    }
    fn on_submit(&self) {
        self.on_cancel();
        self.emit_submit();
    }
}

impl Default for RemoveRow {
    fn default() -> Self {
        Self::new()
    }
}
