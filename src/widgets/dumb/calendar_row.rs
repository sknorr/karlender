use super::RemoveRow;
use crate::domain::*;
use adw::prelude::ComboRowExt;
use adw::subclass::prelude::{ExpanderRowImpl, PreferencesRowImpl};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use glib::SignalHandlerId;
use gtk::prelude::*;
use gtk::subclass::list_box_row::ListBoxRowImpl;
use libadwaita as adw;
use std::cell::Cell;

#[widget(extends adw::ExpanderRow)]
#[template(file = "calendar_row.ui")]
pub struct CalendarRow {
    #[property_string]
    pub calendar_id: Cell<String>,
    #[property_bool]
    pub calendar_remote: Cell<bool>,
    #[property_bool]
    pub calendar_default: Cell<bool>,
    #[property_bool]
    pub calendar_enabled: Cell<bool>,
    #[property_string]
    pub calendar_color: Cell<String>,

    #[template_child]
    pub name_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub name_entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub enabled_switch: TemplateChild<gtk::Switch>,
    #[template_child]
    pub default_switch: TemplateChild<gtk::Switch>,
    #[template_child]
    pub color_row: TemplateChild<adw::ComboRow>,
    #[template_child]
    pub remove_row: TemplateChild<RemoveRow>,

    #[callback(noargs)]
    on_remove: (),

    #[signal]
    remove: (),
    #[signal]
    change_color: (),
    #[signal]
    change_name: (),
    #[signal]
    change_default: (),
    #[signal]
    change_enabled: (),
}

impl CalendarRow {
    pub fn new(calendar: &Calendar) -> Self {
        glib::Object::new(&[
            ("title", &calendar.name),
            ("calendar-remote", &calendar.is_remote),
            ("calendar-id", &calendar.id),
            ("calendar-default", &calendar.default),
            ("calendar-enabled", &calendar.enabled),
            ("calendar-color", &calendar.color.to_string()),
        ])
        .expect("Failed to create CalendarRow")
    }

    pub fn constructed(&self) {
        self.connect_realize(|s| s.realized());
    }

    pub fn realized(&self) {
        let _self = self;
        let calendar_default: bool = self.property("calendar-default");
        let calendar_enabled: bool = self.property("calendar-enabled");
        let calendar_color: String = self.property("calendar-color");
        let remote = self.property("calendar-remote");

        self.enabled_switch().set_state(calendar_enabled);
        self.default_switch().set_state(calendar_default);

        if calendar_default {
            self.set_property("subtitle", &gettext("Default calendar"))
        }
        if remote {
            self.name_row().set_visible(false);
            self.remove_row().set_visible(false);
        } else {
            self.name_entry()
                .connect_changed(glib::clone!(@weak _self => move |_e| {
                    _self.emit_change_name();
                }));
        }
        self.enabled_switch()
            .connect_state_notify(glib::clone!(@weak _self => move |_sw| {
                _self.emit_change_enabled();
            }));

        self.default_switch()
            .connect_state_notify(glib::clone!(@weak _self => move |_sw| {
                _self.emit_change_default();
            }));

        let color_row = &self.imp().color_row;

        let model = gdk4::gio::ListStore::new(ColorModel::gobject_type());
        model.append(&ColorModel::from(KarlenderColor::Red).to_gobject());
        model.append(&ColorModel::from(KarlenderColor::Green).to_gobject());
        model.append(&ColorModel::from(KarlenderColor::Blue).to_gobject());
        model.append(&ColorModel::from(KarlenderColor::Orange).to_gobject());
        model.append(&ColorModel::from(KarlenderColor::Black).to_gobject());
        color_row.set_expression(Some(&ColorModel::property_expression("name")));
        color_row.set_model(Some(&model));

        color_row.set_selected(ColorModel::index_of_str(&calendar_color));

        color_row.connect_selected_notify(glib::clone!(@weak _self => move |_row| {
            _self.emit_change_color();
        }));
    }

    pub fn calendar_id(&self) -> String {
        self.property("calendar-id")
    }

    pub fn is_enabled(&self) -> bool {
        self.enabled_switch().state()
    }
    pub fn is_default(&self) -> bool {
        self.default_switch().state()
    }
    pub fn color(&self) -> KarlenderColor {
        let color = self
            .color_row()
            .selected_item()
            .unwrap()
            .property::<String>("id");
        color.into()
    }

    pub fn name(&self) -> String {
        self.name_entry().text().to_string()
    }

    fn on_remove(&self) {
        self.emit_remove()
    }

    pub fn connect_remove(&self, f: impl Fn(&Self, String) + 'static) -> SignalHandlerId {
        let calendar_id: String = self.property("calendar-id");
        self._connect_remove(move |s| f(s, calendar_id.clone()))
    }

    pub fn connect_change_name(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_change_name(f)
    }
    pub fn connect_change_default(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_change_default(f)
    }
    pub fn connect_change_color(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_change_color(f)
    }
    pub fn connect_change_enabled(&self, f: impl Fn(&Self) + 'static) -> SignalHandlerId {
        self._connect_change_enabled(f)
    }
}

impl PreferencesRowImpl for imp::CalendarRow {}

impl ListBoxRowImpl for imp::CalendarRow {}

#[gobject(id, name)]
struct ColorModel {
    id: String,
    name: String,
}
impl ColorModel {
    fn _index_of(color: KarlenderColor) -> u32 {
        match color {
            KarlenderColor::Red => 0,
            KarlenderColor::Green => 1,
            KarlenderColor::Blue => 2,
            KarlenderColor::Orange => 3,
            KarlenderColor::Black => 4,
        }
    }
    fn index_of_str(color: &str) -> u32 {
        match color {
            "Red" => 0,
            "Green" => 1,
            "Blue" => 2,
            "Orange" => 3,
            "Black" => 4,
            _ => 5,
        }
    }
}
impl From<KarlenderColor> for ColorModel {
    fn from(c: KarlenderColor) -> Self {
        match c {
            KarlenderColor::Red => ColorModel {
                id: "Red".into(),
                name: gettext("Red"),
            },
            KarlenderColor::Green => ColorModel {
                id: "Green".into(),
                name: gettext("Green"),
            },
            KarlenderColor::Blue => ColorModel {
                id: "Blue".into(),
                name: gettext("Blue"),
            },
            KarlenderColor::Orange => ColorModel {
                id: "Orange".into(),
                name: gettext("Orange"),
            },
            KarlenderColor::Black => ColorModel {
                id: "Black".into(),
                name: gettext("Black"),
            },
        }
    }
}
