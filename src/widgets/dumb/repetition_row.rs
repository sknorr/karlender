use adw::subclass::prelude::{ExpanderRowImpl, PreferencesRowImpl};
use adw::traits::{ComboRowExt, PreferencesRowExt};
use gdk4::gio::ListStore;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::list_box_row::ListBoxRowImpl;
use libadwaita as adw;
use std::cell::Cell;
use std::collections::HashMap;

use crate::adapters::*;
use crate::domain::{DatetimeFormat, KarlenderDateTime, KarlenderTimezone, Repeat};
use crate::store::settings::DateFormats;
use crate::widgets::DatetimeChooser;

#[widget(extends libadwaita::ExpanderRow)]
#[skip_auto_impl]
#[template(file = "repetition_row.ui")]
pub struct RepetitionRow {
    #[template_child]
    pub repeat_switch: TemplateChild<gtk::Switch>,
    #[template_child]
    pub freq_combo_row: TemplateChild<adw::ComboRow>,
    #[template_child]
    pub by_day_action_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub by_day_mo_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_tu_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_we_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_th_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_fr_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_sa_button: TemplateChild<gtk::ToggleButton>,
    #[template_child]
    pub by_day_su_button: TemplateChild<gtk::ToggleButton>,

    #[template_child]
    pub interval_action_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub interval_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub interval_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub end_combo_row: TemplateChild<adw::ComboRow>,
    #[template_child]
    pub end_count_adjustment: TemplateChild<gtk::Adjustment>,
    #[template_child]
    pub end_count_action_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub end_date_action_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub end_count_spin_button: TemplateChild<gtk::SpinButton>,
    #[template_child]
    pub end_date_chooser: TemplateChild<DatetimeChooser>,

    #[property_bool]
    pub repeat: Cell<bool>,

    pub dateformat: Cell<Option<DateFormats>>,
    pub timezone: Cell<Option<KarlenderTimezone>>,
    pub today: Cell<Option<KarlenderDateTime>>,

    #[signal]
    changed: (),
}

impl ListBoxRowImpl for imp::RepetitionRow {}
impl PreferencesRowImpl for imp::RepetitionRow {}
impl ExpanderRowImpl for imp::RepetitionRow {}

const FREQ_VALUES: [&str; 5] = [
    RRULE_VALUE_YEARLY,
    RRULE_VALUE_MONTHLY,
    RRULE_VALUE_WEEKLY,
    RRULE_VALUE_DAILY,
    RRULE_VALUE_HOURLY,
];

const END_VALUES: [&str; 3] = ["Never", RRULE_FIELD_COUNT, RRULE_FIELD_UNTIL];

impl RepetitionRow {
    pub fn new() -> Self {
        glib::Object::new(&[("repeat", &false)]).expect("Failed to crate RepetitionRow")
    }

    pub fn constructed(&self) {
        self.imp().today.set(Some(KarlenderTimezone::local().now()));
        self.connect_realize(|s| s.realized());
    }

    pub fn set_date_formatting_info(
        &self,
        dateformat: DateFormats,
        timezone: Option<KarlenderTimezone>,
    ) {
        self.imp().end_date_chooser.set_date(
            dateformat.date,
            &timezone,
            KarlenderTimezone::local().now(),
        );
        self.imp().dateformat.set(Some(dateformat));
        self.imp().timezone.set(timezone);
    }

    fn realized(&self) {
        let _self = self;
        let by_day_action_row = &self.imp().by_day_action_row;
        let repeat_switch = &self.imp().repeat_switch;

        repeat_switch.set_state(self.property("repeat"));
        repeat_switch.connect_state_notify(glib::clone!(@weak _self => move |sw| {
            if sw.state() {
                let selected_name: String = _self.freq_combo_row().selected_item().unwrap().property("name");
                _self.set_property("subtitle", selected_name);
            } else {
                _self.set_property("subtitle", "");
            }
            _self.emit_changed();
        }));

        self.fix_by_day_content_alignment(by_day_action_row);

        self.set_visibilities(RRULE_VALUE_YEARLY);

        self.init_freq_row();
        self.init_by_day_row();
        self.init_interval_row();
        self.init_end_row();
    }

    fn init_freq_row(&self) {
        let _self = self;
        let freq_combo_row = &self.imp().freq_combo_row;
        let freq_model = ListStore::new(Pair::gobject_type());
        for value in FREQ_VALUES {
            freq_model.append(
                &Pair::new(
                    value,
                    match value {
                        RRULE_VALUE_YEARLY => gettext("Yearly"),
                        RRULE_VALUE_MONTHLY => gettext("Monthly"),
                        RRULE_VALUE_WEEKLY => gettext("Weekly"),
                        RRULE_VALUE_DAILY => gettext("Daily"),
                        RRULE_VALUE_HOURLY => gettext("Hourly"),
                        _ => "".into(),
                    },
                )
                .to_gobject(),
            );
        }
        freq_combo_row.set_expression(Some(Pair::property_expression("name")));
        freq_combo_row.set_model(Some(&freq_model));
        freq_combo_row.connect_selected_item_notify(glib::clone!(@weak _self => move |cr| {
            let selected_name: String = cr.selected_item().unwrap().property("name");
            _self.set_property("subtitle", &selected_name);
            let selected: String = cr.selected_item().unwrap().property("id");
            _self.set_visibilities(selected.as_str());
            _self.emit_changed();
        }));
    }

    fn init_by_day_row(&self) {
        let _self = self;
        self.by_day_mo_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_tu_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_we_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_th_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_fr_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_sa_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
        self.by_day_su_button()
            .connect_active_notify(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
    }

    fn init_interval_row(&self) {
        let s = self;
        self.imp()
            .interval_adjustment
            .connect_value_changed(glib::clone!(@weak s => move |_| {
                s.emit_changed()
            }));
    }

    fn init_end_row(&self) {
        let _self = self;
        let end_combo_row = self.end_combo_row();
        let end_date_chooser = self.end_date_chooser();

        let end_model = ListStore::new(Pair::gobject_type());
        end_model.append(&Pair::new(END_VALUES[0], gettext("Never")).to_gobject());
        end_model.append(&Pair::new(END_VALUES[1], gettext("Fixed count")).to_gobject());
        end_model.append(&Pair::new(END_VALUES[2], gettext("On date")).to_gobject());
        end_combo_row.set_expression(Some(Pair::property_expression("name")));
        end_combo_row.set_model(Some(&end_model));
        end_combo_row.connect_selected_notify(glib::clone!(@weak _self => move |row| {
            let selected_id: String = row.selected_item().unwrap().property("id");
            match selected_id.as_str() {
                RRULE_FIELD_COUNT => {
                    _self.end_count_action_row().set_visible(true);
                    _self.end_date_action_row().set_visible(false);
                }
                RRULE_FIELD_UNTIL => {
                    _self.end_count_action_row().set_visible(false);
                    _self.end_date_action_row().set_visible(true);
                }
                _ => {
                    _self.end_count_action_row().set_visible(false);
                    _self.end_date_action_row().set_visible(false);
                }
            }
            _self.emit_changed();
        }));

        end_date_chooser.connect_changed(glib::clone!(@weak _self => move |_| {
            _self.emit_changed();
        }));

        self.end_count_adjustment()
            .connect_value_changed(glib::clone!(@weak _self => move |_| {
                _self.emit_changed();
            }));
    }

    fn fix_by_day_content_alignment(&self, by_day_action_row: &adw::ActionRow) {
        let title_box = by_day_action_row
            .first_child()
            .unwrap()
            .first_child()
            .unwrap()
            .next_sibling()
            .unwrap()
            .next_sibling()
            .unwrap();
        title_box.set_visible(false);
        let suffix_box = title_box.next_sibling().unwrap();
        let suffix_box = suffix_box.downcast::<gtk::Box>().unwrap();
        suffix_box.set_hexpand(true);
    }

    pub fn get_repeat_value(&self) -> Option<Repeat> {
        let freq = self
            .freq_combo_row()
            .selected_item()
            .map(|o| o.property::<String>("id"));
        let mut by_day = Vec::new();
        if self.imp().by_day_mo_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_MONDAY);
        }
        if self.imp().by_day_tu_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_TUESDAY);
        }
        if self.imp().by_day_we_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_WEDNESDAY);
        }
        if self.imp().by_day_th_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_THURSDAY);
        }
        if self.imp().by_day_fr_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_FRIDAY);
        }
        if self.imp().by_day_sa_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_SATURDAY);
        }
        if self.imp().by_day_su_button.is_active() {
            by_day.push(RRULE_VALUE_BYDAY_SUNDAY);
        }
        let interval = (self.imp().interval_adjustment.value() as u32).to_string();
        let end = self
            .end_combo_row()
            .selected_item()
            .map(|o| o.property::<String>("id"));
        let mut map = HashMap::new();
        if let Some(freq) = freq {
            map.insert(RRULE_FIELD_FREQ.to_string(), freq);
        }
        map.insert(RRULE_FIELD_INTERVAL.to_string(), interval);
        map.insert(RRULE_FIELD_BYDAY.to_string(), by_day.join(","));
        let end_date: String = self
            .end_date_chooser()
            .date()
            .format(DatetimeFormat::ISO8601);
        let end_count: String = (self.imp().end_count_adjustment.value() as u32).to_string();
        if let Some(end) = end {
            match end.as_str() {
                "After" => {
                    map.insert(RRULE_FIELD_COUNT.to_string(), end_count);
                }
                "OnDate" => {
                    map.insert(RRULE_FIELD_UNTIL.to_string(), end_date);
                }
                _ => {}
            }
        }
        if self.imp().repeat_switch.state() {
            Some(map)
        } else {
            None
        }
    }

    fn set_visibilities(&self, freq: &str) {
        match freq {
            RRULE_VALUE_YEARLY => {
                self.by_day_action_row().set_visible(false);
                self.interval_action_row().set_visible(false);
            }
            RRULE_VALUE_MONTHLY => {
                self.by_day_action_row().set_visible(false);
                self.interval_action_row().set_visible(true);
                self.interval_action_row()
                    .set_title(&gettext("Every n months"))
            }
            RRULE_VALUE_WEEKLY => {
                self.by_day_action_row().set_visible(true);
                self.interval_action_row().set_visible(true);
                self.interval_action_row()
                    .set_title(&gettext("Every n weeks"))
            }
            RRULE_VALUE_DAILY => {
                self.by_day_action_row().set_visible(false);
                self.interval_action_row().set_visible(true);
                self.interval_action_row()
                    .set_title(&gettext("Every n days"))
            }
            RRULE_VALUE_HOURLY => {
                self.by_day_action_row().set_visible(false);
                self.interval_action_row().set_visible(true);
                self.interval_action_row()
                    .set_title(&gettext("Every n hours"))
            }
            _ => {}
        }
    }

    pub fn set_state(&self, rep: Option<&Repeat>) {
        if rep.is_none() {
            self.repeat_switch().set_state(false);
            return;
        } else {
            self.repeat_switch().set_state(true);
        }
        for (k, v) in rep.unwrap() {
            if k.as_str() == RRULE_FIELD_FREQ {
                if let Some(i) = FREQ_VALUES.iter().position(|k| k == v) {
                    self.freq_combo_row().set_selected(i as u32);
                }
            } else if k.as_str() == RRULE_FIELD_INTERVAL {
                if let Ok(u) = v.parse::<u32>() {
                    self.interval_adjustment().set_value(u as f64);
                }
            } else if k.as_str() == RRULE_FIELD_COUNT {
                if let Ok(u) = v.parse::<u32>() {
                    if let Some(i) = END_VALUES.iter().position(|k| *k == RRULE_FIELD_COUNT) {
                        self.end_combo_row().set_selected(i as u32);
                    }
                    self.end_count_adjustment().set_value(u as f64);
                }
            } else if k.as_str() == RRULE_FIELD_UNTIL {
                if let Some(i) = END_VALUES.iter().position(|k| *k == RRULE_FIELD_UNTIL) {
                    self.end_combo_row().set_selected(i as u32);
                }
                println!("v {}", v);
                // self.end_date_chooser().set_date();
            }
        }
    }

    pub fn on_save(&self, _button: gtk::Widget) {
        // TODO
    }
}

#[gobject(id, name)]
struct Pair {
    id: String,
    name: String,
}

impl Pair {
    pub fn new(id: &str, name: String) -> Self {
        Self {
            id: id.into(),
            name,
        }
    }
}

impl Default for RepetitionRow {
    fn default() -> Self {
        Self::new()
    }
}
