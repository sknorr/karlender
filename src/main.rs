#[macro_use]
extern crate log;
#[macro_use]
extern crate gtk_rust_app;

use crate::{
    header_buttons::*,
    pages::*,
    store::{
        calendars::CalDAVSyncMiddleware, persistence::LocalPersistenceMiddleware,
        selection::SelectionMiddleware, ui::NotificationMiddleware,
    },
    widgets::EventsSidebar,
};
use adw::StyleManager;
use gdk4::{
    gio::SimpleAction,
    prelude::{ActionMapExt, ApplicationExt},
};
use gettextrs::gettext;
use glib::Cast;
use gtk::{
    prelude::WidgetExt,
    traits::{GtkApplicationExt, GtkWindowExt},
};
use gtk_rust_app::widgets::LeafletLayout;
use libadwaita as adw;
use store::{
    event_editing::EditorMiddleware,
    gstore::{dispatch, select},
    store, State,
};

mod adapters;
mod domain;
mod header_buttons;
mod migration;
mod pages;
mod store;
mod widgets;

fn main() {
    env_logger::init();

    store::init_store(
        State::init(),
        store::reduce,
        vec![
            EditorMiddleware::new(),
            CalDAVSyncMiddleware::new(),
            NotificationMiddleware::new(),
            LocalPersistenceMiddleware::new(),
            SelectionMiddleware::new(),
        ],
    );

    gtk_rust_app::builder::builder(
        include_bytes!("../Cargo.toml"),
        include_bytes!("../target/gra-gen/compiled.gresource"),
    )
    .store(store::store())
    .styles(include_str!("styles.css"))
    .build(
        move |application, _project_descriptor, settings| {
            let style_manager = adw::StyleManager::default();
            style_manager.set_color_scheme(adw::ColorScheme::PreferLight);

            widgets::init();

            let leaflet_layout = LeafletLayout::builder(settings)
                .add_page(MonthPage::new())
                .add_page(AboutPage::new())
                .add_page(SettingsPage::new())
                .add_page(EditorPage::new())
                .add_page(AddAccountPage::new())
                .add_page(DayPage::new())
                .add_main_header_start(BackHeaderButton::new())
                .add_main_header_start(NewEventHeaderButton::new())
                .add_main_header_start(SaveHeaderButton::new())
                .add_main_header_start(NavHeaderButton::new())
                .add_main_header_end(MenuHeaderButton::new())
                .add_main_header_end(SpinnerHeaderButton::new())
                .build();

            leaflet_layout
                .get_leaflet()
                .set_fold_threshold_policy(adw::FoldThresholdPolicy::Minimum);

            let main_leaflet = leaflet_layout.get_leaflet();

            leaflet_layout
                .get_sidebar_scrolled_window()
                .set_height_request(200);
            leaflet_layout
                .get_sidebar_scrolled_window()
                .set_vexpand(false);
            let sidebar_leaflet = leaflet_layout.get_sidebar_content();

            sidebar_leaflet.append(
                &gtk::Separator::builder()
                    .orientation(gtk::Orientation::Horizontal)
                    .build(),
            );
            sidebar_leaflet.append(&EventsSidebar::new());

            sidebar_leaflet.connect_folded_notify(
                glib::clone!(@weak main_leaflet => move |sidebar_leaflet| {
                    let folded = main_leaflet.is_folded() || sidebar_leaflet.is_folded();
                    dispatch!(crate::store::_CHANGE_MOBILE, folded);
                }),
            );

            main_leaflet.connect_folded_notify(
                glib::clone!(@weak sidebar_leaflet => move |main_leaflet| {
                    let folded = main_leaflet.is_folded() || sidebar_leaflet.is_folded();
                    dispatch!(crate::store::_CHANGE_MOBILE, folded);
                }),
            );

            select!(|state| state.ui => glib::clone!(@weak leaflet_layout => move |state| {
                on_state_ui_update(state, leaflet_layout, &style_manager);
            }));

            let view_stack = leaflet_layout.get_view_stack();
            handle_navigation(view_stack);

            handle_notification(&leaflet_layout);
            let window = gtk_rust_app::window(
                application,
                gettext("Calendar"),
                settings,
                leaflet_layout.upcast_ref(),
            );
            // use notify_rust::Notification;
            // Notification::new()
            //     .id("codes.loers.Karlender")
            //     .summary("Calendar")
            //     .body("Karlender started.")
            //     .icon("codes.loers.Karlender")
            //     .show().unwrap();
            // TODO: Run karlender in background
            // TODO: Implement alarms
            window.set_hide_on_close(true);

            if std::env::var("KARLENDER_BG").is_err() {
                window.show();
            }

            if let Some(action) = application.lookup_action(crate::store::CLOSE) {
                let simple_action: SimpleAction = action.downcast().unwrap();
                simple_action.connect_activate(glib::clone!(@weak application => move |_, _| {
                    if let Some(w) = application.windows().first() {
                        w.hide();
                    }
                }));
            } else {
                error!("Action '{}' not found.", crate::store::CLOSE);
            }
            if let Some(action) = application.lookup_action(crate::store::QUIT) {
                let simple_action: SimpleAction = action.downcast().unwrap();
                simple_action.connect_activate(glib::clone!(@weak application => move |_, _| {
                    application.quit();
                }));
            } else {
                error!("Action '{}' not found.", crate::store::CLOSE);
            }
        },
        |application, _project_descriptor, _settings| {
            if let Some(w) = application.windows().first() {
                if w.is_visible() {
                    // nothing to do
                } else {
                    w.present();
                }
            }
        },
    );
}

fn on_state_ui_update(state: &State, leaflet_layout: LeafletLayout, style_manager: &StyleManager) {
    let hm = leaflet_layout.get_main_header();
    let hs = leaflet_layout.get_sidebar_header();

    if state.ui.dark {
        style_manager.set_color_scheme(adw::ColorScheme::PreferDark);
    } else {
        style_manager.set_color_scheme(adw::ColorScheme::PreferLight);
    }

    if state.ui.mobile {
        hm.add_css_class("flat");
        hs.add_css_class("flat");
        if let Some(title_label) = hm
            .title_widget()
            .and_then(|w| w.downcast::<gtk::Label>().ok())
        {
            title_label.set_ellipsize(gdk4::pango::EllipsizeMode::End);
            title_label.set_label(&state.ui.title);
            title_label.add_css_class("title-3");
        } else {
            let title_label = gtk::Label::new(Some(&state.ui.title));
            title_label.set_ellipsize(gdk4::pango::EllipsizeMode::End);
            title_label.add_css_class("title-3");
            hm.set_title_widget(Some(&title_label));
        }
    } else {
        hm.remove_css_class("flat");
        hs.remove_css_class("flat");
        let title = gettext("Calendar");
        if let Some(title_label) = hm
            .title_widget()
            .and_then(|w| w.downcast::<gtk::Label>().ok())
        {
            title_label.set_ellipsize(gdk4::pango::EllipsizeMode::End);
            title_label.set_label(&title);
            title_label.remove_css_class("title-3");
        } else {
            let title_label = gtk::Label::new(Some(&title));
            title_label.set_ellipsize(gdk4::pango::EllipsizeMode::End);
            hm.set_title_widget(Some(&title_label));
        }
    }
}

fn handle_navigation(view_stack: &adw::ViewStack) {
    view_stack.connect_visible_child_name_notify(|v| {
        dispatch!(
            crate::store::_UPDATE_HISTORY,
            v.visible_child_name().unwrap().to_string()
        )
    });

    select!(|state| state.ui.current_page => glib::clone!(@weak view_stack => move |state| {
        let page = &state.ui.current_page;
        view_stack.set_visible_child_name(page);
    }));
}

fn handle_notification(leaflet_layout: &LeafletLayout) {
    select!(|state| state.ui.in_app_notification => glib::clone!(@weak leaflet_layout => move |state| {
        if let Some(notification) = &state.ui.in_app_notification {
            let toast = adw::Toast::new(&notification.text);
            if let Some((action_label, action_name, _action_value)) = &notification.action {
                toast.set_button_label(Some(action_label));
                toast.set_action_name(Some(action_name));
            }
            toast.connect_dismissed(|_| {
                dispatch!(crate::store::_CLEAR_NOTIFICATION);
            });
            leaflet_layout.show_toast(&toast);
        }
    }));
}
