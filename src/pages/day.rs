use crate::domain::*;
use crate::store::{gstore::*, *};
use crate::widgets::EventRow;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use gtk::builders::GestureSwipeBuilder;
use gtk::prelude::*;
use gtk_rust_app::widgets::Page;
use libadwaita as adw;
use std::cell::Cell;

pub const DAY_PAGE_NAME: &str = "day";

#[widget(extends gtk::Box)]
#[template(file = "day.ui")]
pub struct DayPage {
    #[property_string]
    pub title: Cell<String>,

    pub date: Cell<Option<KarlenderDate>>,

    #[template_child]
    pub clamp: TemplateChild<adw::Clamp>,
    #[template_child]
    pub event_box: TemplateChild<gtk::Box>,
    #[template_child]
    pub day_label: TemplateChild<gtk::Label>,

    #[callback(noargs)]
    on_add_clicked: (),
}
impl DayPage {
    pub fn new() -> Self {
        let self_: Self = glib::Object::new(&[]).expect("Failed to create DayPage");
        self_
            .imp()
            .date
            .set(Some(KarlenderTimezone::local().today()));
        self_
    }

    pub fn constructed(&self) {
        let self_ = self;

        let swipe = GestureSwipeBuilder::new().build();
        swipe.connect_swipe(|_gs, x, _y| {
            if x > 500. {
                dispatch!(crate::store::_GO_PREV)
            } else if x < -500. {
                dispatch!(crate::store::_GO_NEXT)
            }
        });
        self_.add_controller(&swipe);

        self.connect_realize(glib::clone!(@weak self_ => move |_| self_.realized()));
    }

    fn realized(&self) {
        let s = self;
        select!(|state| state.ui.mobile => glib::clone!(@weak s => move |state| {
            if state.ui.mobile {
                s.imp().day_label.set_visible(false);
            } else {
                s.imp().day_label.set_visible(true);
            }
        }));

        select!(|state|
            state.calendar_slice,
            state.selection_slice,
            state.settings,
        => glib::clone!(@weak s => move |state| {
            s.on_state_update(state)
        }));
    }

    fn on_state_update(&self, state: &State) {
        let dateformat = &state.settings.dateformat;
        let timezone = state.settings.timezone();
        let selection = &state.selection_slice.selection;
        let former_selection = &state.selection_slice.former_selection;
        let calendars = &state.calendar_slice.calendars;

        let date_from_selection = selection
            .as_ref()
            .or(former_selection.as_ref())
            .map(|s| s.start.date());
        let date_from_state = self.imp().date.take();
        if date_from_selection.is_some() {
            self.imp().date.set(date_from_selection.clone());
        };
        if let Some(date) = date_from_selection.or(date_from_state) {
            let event_box = self.event_box();
            if let Some(df) = dateformat {
                self.set_property("title", date.format(df.date));
            }
            while let Some(child) = event_box.first_child() {
                if child.next_sibling().is_some() {
                    event_box.remove(&child);
                } else {
                    break;
                }
            }
            let mut events = Vec::new();
            for (calendar_id, cal) in calendars {
                for event in cal.events.values() {
                    if event.contains_date(&date) {
                        events.push((calendar_id.to_string(), event));
                    }
                }
            }
            events.sort_by(|(_, a), (_, b)| b.start.cmp(&a.start));
            for (calendar_id, event) in events {
                let event_row = EventRow::new(
                    dateformat.clone(),
                    timezone.clone(),
                    &calendar_id,
                    event,
                    false,
                );
                event_row.connect_remove(|r| {
                    dispatch!(crate::store::REMOVE_EVENT, (r.calendar_id(), r.event_id()));
                });
                event_row.connect_select(|r| {
                    dispatch!(crate::store::EDIT_EVENT, (r.calendar_id(), r.event_id()));
                    dispatch!(crate::store::NAVIGATE, crate::pages::EDITOR_PAGE_NAME);
                });
                event_box.prepend(&event_row);
            }
        }
    }

    pub fn title(&self) -> String {
        self.property("title")
    }

    fn on_add_clicked(&self) {
        dispatch!(crate::store::NEW_EVENT);
    }
}

impl Page for DayPage {
    fn name(&self) -> &'static str {
        DAY_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        Some((gettext("Day"), "format-justify-fill-symbolic".into()))
    }
}

impl Default for DayPage {
    fn default() -> Self {
        Self::new()
    }
}
