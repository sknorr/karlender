use crate::domain::*;
use crate::widgets::RepetitionRow;
use crate::{
    store::{gstore::*, store, State},
    widgets::DatetimeChooser,
};
use adw::traits::ComboRowExt;
use gdk4::gio::ListStore;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;
use gtk_rust_app::widgets::Page;
use libadwaita as adw;
use std::cell::Cell;
use std::sync::Mutex;

pub const EDITOR_PAGE_NAME: &str = "edit-event";

#[widget(extends gtk::Box)]
#[template(file = "editor.ui")]
pub struct EditorPage {
    #[template_child]
    pub name_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub name_entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub location_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub location_entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub calendar_row: TemplateChild<adw::ComboRow>,

    #[template_child]
    pub full_day_switch: TemplateChild<gtk::Switch>,

    #[template_child]
    pub start_row: TemplateChild<adw::ExpanderRow>,
    #[template_child]
    pub start_chooser: TemplateChild<DatetimeChooser>,
    #[template_child]
    pub end_row: TemplateChild<adw::ExpanderRow>,
    #[template_child]
    pub end_chooser: TemplateChild<DatetimeChooser>,
    #[template_child]
    pub save_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub repetition_row: TemplateChild<RepetitionRow>,
    #[template_child]
    pub notes_text_view: TemplateChild<gtk::TextView>,

    #[template_child]
    pub entries_size_group: TemplateChild<gtk::SizeGroup>,

    #[property_bool]
    pub full_day: Cell<bool>,

    pub event: Cell<Option<Event>>,

    #[callback]
    on_save: (),
}

impl EditorPage {
    pub fn new() -> Self {
        glib::Object::new(&[("full-day", &false)]).expect("Failed to create EditorPage")
    }

    pub fn constructed(&self) {
        let s = self;
        let size_group = &self.imp().entries_size_group;
        let name_row = &self.imp().name_row;
        let location_row = &self.imp().location_row;
        let calendar_row = &self.imp().calendar_row;

        size_group.add_widget(
            &location_row
                .child()
                .unwrap()
                .last_child()
                .unwrap()
                .prev_sibling()
                .unwrap(),
        );
        size_group.add_widget(
            &name_row
                .child()
                .unwrap()
                .last_child()
                .unwrap()
                .prev_sibling()
                .unwrap(),
        );

        let calendars_model = gdk4::gio::ListStore::new(Calendar::gobject_type());
        calendar_row.set_model(Some(&calendars_model));
        calendar_row.set_expression(Some(&Calendar::property_expression("name")));

        select_state!(
            "calendars without events",
            |state| {
                let mut selected_state = State::default();
                for (id, c) in &state.calendar_slice.calendars {
                    let calendar = Calendar {
                        id: c.id.clone(),
                        name: c.name.clone(),
                        color: c.color,
                        default: c.default,
                        enabled: c.enabled,
                        events: Default::default(),
                        is_remote: false,
                    };
                    selected_state
                        .calendar_slice
                        .calendars
                        .insert(id.clone(), calendar);
                }
                selected_state
            },
            glib::clone!(@weak s => move |state| {
                s.update_calendars_from_state(state, &calendars_model)
            })
        );

        select!(|state|
            state.settings,
            state.editor.event,
        => glib::clone!(@weak s => move |state| {
            s.update_event_from_state(state)
        }));

        select!(|state|
            state.ui,
        => glib::clone!(@weak s => move |state| {
            s.save_button().set_visible(state.ui.mobile);
        }));

        let (sender, receiver) = glib::MainContext::channel::<()>(glib::PRIORITY_DEFAULT);
        receiver.attach(
            None,
            glib::clone!(@weak s => @default-return glib::Continue(true), move |_| {
                s.save();
                glib::Continue(true)
            }),
        );
        EDITOR_SAVE
            .set(Mutex::new(sender))
            .expect("Could not prepare editor save sender for external save calls");
    }

    fn update_calendars_from_state(&self, state: &State, calendars_model: &ListStore) {
        let calendar_row = self.calendar_row();
        let calendars = &state.calendar_slice.calendars;

        calendars_model.remove_all();

        for (index, c) in calendars.values().enumerate() {
            calendars_model.append(&c.to_gobject());
            unsafe { calendar_row.set_data(&c.id, index) }
        }
    }

    fn update_event_from_state(&self, state: &State) {
        let event = &state.editor.event;

        let calendar_row = self.calendar_row();
        let name_entry = self.name_entry();
        let location_entry = self.location_entry();
        let start_chooser = self.start_chooser();
        let end_chooser = self.end_chooser();
        let full_day_switch = self.full_day_switch();
        let repetition_row = self.repetition_row();
        let notes_text_view = self.notes_text_view();

        let timezone = state.settings.timezone();

        if let Some(dateformat) = &state.settings.dateformat {
            repetition_row.set_date_formatting_info(dateformat.clone(), timezone.clone());
            if let Some(event) = event.as_ref() {
                debug!("Editor: Update event: {:?}", event);
                let local_event = self.imp().event.take();
                if local_event.is_some() && local_event.as_ref().unwrap() == event {
                    self.imp().event.set(local_event);
                    return;
                }

                unsafe {
                    let index = calendar_row
                        .data::<u32>(&event.calendar_url)
                        .unwrap()
                        .as_ref();
                    calendar_row.set_selected(*index);
                }
                if name_entry.text() != event.name {
                    name_entry.set_text(&event.name);
                }
                if let Some(location) = event.location.as_ref() {
                    if &location_entry.text().to_string() != location {
                        location_entry.set_text(location);
                    }
                } else {
                    location_entry.set_text("");
                }
                if event.full_day {
                    start_chooser.set_date(dateformat.date, &timezone, event.start.clone());
                    end_chooser.set_date(
                        dateformat.date,
                        &timezone,
                        event.end.clone() - KarlenderDateTimeDuration::days(1),
                    );
                } else {
                    start_chooser.set_date(dateformat.datetime, &timezone, event.start.clone());
                    end_chooser.set_date(dateformat.datetime, &timezone, event.end.clone());
                }
                end_chooser.set_full_day(event.full_day);
                start_chooser.set_full_day(event.full_day);
                full_day_switch.set_state(event.full_day);
                repetition_row.set_state(event.repeat.as_ref());
                if let Some(notes) = &event.notes {
                    let buffer = notes_text_view.buffer();
                    buffer.set_text(notes.replace("\\n", "\n").as_str());
                } else {
                    let buffer = notes_text_view.buffer();
                    buffer.set_text("");
                }
                self.imp().event.set(state.editor.event.clone())
            }
        }
    }

    fn name(&self) -> String {
        return self.imp().name_entry.text().into();
    }

    fn calendar_id(&self) -> String {
        self.imp()
            .calendar_row
            .selected_item()
            .unwrap()
            .property("id")
    }

    fn location(&self) -> Option<String> {
        let location = self.imp().location_entry.text();
        if location.is_empty() {
            None
        } else {
            Some(location.into())
        }
    }

    fn full_day(&self) -> bool {
        self.imp().full_day_switch.state()
    }

    fn start(&self) -> KarlenderDateTime {
        self.imp().start_chooser.date()
    }

    fn end(&self) -> KarlenderDateTime {
        self.imp().end_chooser.date()
    }

    fn repeat(&self) -> Option<Repeat> {
        self.imp().repetition_row.get_repeat_value()
    }

    fn notes(&self) -> Option<String> {
        let tb = self.imp().notes_text_view.buffer();
        let start = tb.start_iter();
        let end = tb.end_iter();
        let text = tb.text(&start, &end, true).to_string();
        if text.is_empty() {
            None
        } else {
            Some(text)
        }
    }

    fn on_save(&self, button: gtk::Button) {
        button.dispatch(crate::store::SAVE_EVENT, None);
    }

    // TODO: somewhere in this file I need make that when I create/open the editor, the correct data for the repeat widget is set.

    fn save(&self) {
        if let Some(mut event) = self.imp().event.take() {
            event.name = self.name();
            event.calendar_url = self.calendar_id();
            event.item_url = format!("{}{}", event.calendar_url, event.id);
            event.location = self.location();
            event.full_day = self.full_day();
            event.start = self.start();
            event.end = self.end();
            if event.full_day {
                event.end = event.end + KarlenderDateTimeDuration::days(1)
            }
            event.repeat = self.repeat();
            event.notes = self.notes();
            dispatch!(crate::store::_INTERNAL_SAVE_EVENT, event);
            dispatch!(crate::store::NAVIGATE, crate::store::NAVIGATE_BACK);
        } else {
            warn!("Could not save event: Editor is not initialized.")
        }
    }
}

impl Page for EditorPage {
    fn name(&self) -> &'static str {
        EDITOR_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

pub static EDITOR_SAVE: glib::once_cell::sync::OnceCell<Mutex<glib::Sender<()>>> =
    glib::once_cell::sync::OnceCell::new();

pub fn editor_save() {
    if let Some(mutex) = EDITOR_SAVE.get() {
        if let Ok(lock) = mutex.lock() {
            if let Err(e) = lock.send(()) {
                error!("Could not save: {}", e);
            }
        }
    }
}

impl Default for EditorPage {
    fn default() -> Self {
        Self::new()
    }
}
