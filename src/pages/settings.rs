use crate::domain::*;
use crate::pages::ADD_ACCOUNT_PAGE_NAME;
use crate::store::{gstore::*, store, State};
use crate::widgets::{AccountRow, AddRow, CalendarRow};
use adw::prelude::ComboRowExt;
use adw::traits::{ExpanderRowExt, PreferencesRowExt};
use gdk4::gio::ListStore;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use gtk::prelude::*;
use gtk_rust_app::widgets::Page;
use libadwaita as adw;
use std::collections::HashMap;

pub const SETTINGS_PAGE_NAME: &str = "settings";

#[widget(extends gtk::Box)]
#[template(file = "settings.ui")]
pub struct SettingsPage {
    #[template_child]
    pub timezone_expander_row: TemplateChild<adw::ExpanderRow>,
    #[template_child]
    pub timezones_listbox: TemplateChild<gtk::ListBox>,

    #[template_child]
    pub timezone_search_entry: TemplateChild<gtk::SearchEntry>,

    #[template_child]
    pub dateformat_comborow: TemplateChild<adw::ComboRow>,

    #[template_child]
    pub accounts_list: TemplateChild<gtk::ListBox>,
    #[template_child]
    pub add_account_row: TemplateChild<AddRow>,

    #[template_child]
    pub calendars_list_box: TemplateChild<gtk::ListBox>,
    #[template_child]
    pub add_calendar_row: TemplateChild<adw::ActionRow>,

    #[callback]
    on_add_account: (),

    #[callback]
    on_add_calendar: (),
}

impl SettingsPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create SettingsPage")
    }

    pub fn constructed(&self) {
        self.setup_timezone_row();
        self.setup_dateformat_row();
        let s = self;
        select_state!(
            "calendars without events",
            |state| {
                let mut selected_state = State::default();
                for (id, c) in &state.calendar_slice.calendars {
                    let calendar = Calendar { ..c.clone() };
                    selected_state
                        .calendar_slice
                        .calendars
                        .insert(id.clone(), calendar);
                }
                selected_state
            },
            glib::clone!(@weak s => move |state| {
                s.update_calendars_from_state(state)
            })
        );

        select!(|state| state.settings => glib::clone!(@weak s => move |state| {
            s.update_from_settings(state)
        }));
    }

    fn update_from_settings(&self, state: &State) {
        let tz = state.settings.timezone();
        let df = &state.settings.dateformat;
        if let Some(tz) = tz {
            self.select_timezone(&tz);
        }
        if df.is_some() {
            let pos = crate::store::settings::DateFormats::list()
                .iter()
                .position(|d| d.name == df.as_ref().unwrap().name)
                .unwrap();
            self.imp().dateformat_comborow.set_selected(pos as u32);
        }
        let accounts_list = &self.imp().accounts_list;
        let mut child = accounts_list.last_child().unwrap();
        while let Some(c) = child.prev_sibling() {
            accounts_list.remove(&c);
            child = c;
        }
        // accounts_list.set_visible(false);
        for ac in &state.settings.accounts {
            let acc_row = AccountRow::new(ac);
            acc_row.connect_removed(|_, login| {
                dispatch!(crate::store::REMOVE_ACCOUNT, (login.url, login.login));
            });
            accounts_list.prepend(&acc_row);
        }
        // TODO: Remove this when multiple accounts are supported.
        if !state.settings.accounts.is_empty() {
            self.imp().add_account_row.set_visible(false);
        } else {
            self.imp().add_account_row.set_visible(true);
        }
    }

    fn update_calendars_from_state(&self, state: &State) {
        let calendars = &state.calendar_slice.calendars;

        let mut child = self.imp().calendars_list_box.first_child();
        let mut current_calendar_rows = HashMap::<String, adw::ExpanderRow>::new();
        while let Some(calendar_row) = child {
            child = calendar_row.next_sibling();
            if let Ok(row_cal_id) = calendar_row.try_property::<String>("calendar-id") {
                current_calendar_rows.insert(row_cal_id, calendar_row.downcast().unwrap());
            }
        }

        let mut calendars: Vec<&Calendar> = calendars.values().collect();
        calendars.sort_by(|a, b| a.name.cmp(&b.name));
        for calendar in calendars {
            match current_calendar_rows.remove(&calendar.id) {
                Some(row) => {
                    row.set_title(&calendar.name);
                    if calendar.default {
                        row.set_subtitle(&gettext("Default calendar"));
                    } else {
                        row.set_subtitle("");
                    }
                }
                None => {
                    let calendar_row = CalendarRow::new(calendar);
                    self.calendars_list_box().prepend(&calendar_row);
                    calendar_row.connect_remove(|_row, calendar_id| {
                        dispatch!(crate::store::_REMOVE_CALENDAR, calendar_id);
                    });
                    calendar_row.connect_change_color(|cr| {
                        let mut payload = HashMap::new();
                        payload.insert(CALENDAR_PROP_COLOR, cr.color().to_string().to_variant());
                        dispatch!(crate::store::UPDATE_CALENDAR, (cr.calendar_id(), payload));
                    });
                    calendar_row.connect_change_name(|cr| {
                        let mut payload = HashMap::new();
                        payload.insert(CALENDAR_PROP_NAME, cr.name().to_variant());
                        dispatch!(crate::store::UPDATE_CALENDAR, (cr.calendar_id(), payload));
                    });
                    calendar_row.connect_change_default(|cr| {
                        let mut payload = HashMap::new();
                        payload.insert(CALENDAR_PROP_DEFAULT, cr.is_default().to_variant());
                        dispatch!(crate::store::UPDATE_CALENDAR, (cr.calendar_id(), payload));
                    });
                    calendar_row.connect_change_enabled(|cr| {
                        let mut payload = HashMap::new();
                        payload.insert(CALENDAR_PROP_ENABLED, cr.is_enabled().to_variant());
                        dispatch!(crate::store::UPDATE_CALENDAR, (cr.calendar_id(), payload));
                    });
                }
            }
        }

        for (_, calendar_row) in current_calendar_rows {
            if calendar_row.try_property::<String>("calendar-id").is_ok() {
                self.calendars_list_box().remove(&calendar_row);
            }
        }
    }

    fn on_add_account(&self, _: AddRow) {
        dispatch!(crate::store::NAVIGATE, ADD_ACCOUNT_PAGE_NAME);
    }

    fn on_add_calendar(&self, _: AddRow) {
        dispatch!(crate::store::_ADD_CALENDAR);
    }

    fn setup_timezone_row(&self) {
        let s = self;
        let timezone_search_entry = self.timezone_search_entry();
        let timezones_listbox = self.timezones_listbox();
        let mut group = None;
        timezone_search_entry.connect_changed(glib::clone!(@weak s => move |e| {
            s.search_timezones(e.text().as_str());
        }));
        for t in crate::store::settings::Timezone::list() {
            let b = gtk::CheckButton::builder()
                .height_request(50)
                .label(&t.name)
                .build();
            b.connect_active_notify(|a| {
                if a.is_active() {
                    let name = a.label().unwrap().to_string();
                    dispatch!(crate::store::SELECT_TIMEZONE, name);
                }
            });
            timezones_listbox.append(&b);
            if let Some(group) = &group {
                b.set_group(Some(group));
            } else {
                group = Some(b);
            }
        }
    }

    fn select_timezone(&self, tz: &KarlenderTimezone) {
        self.imp().timezone_expander_row.set_subtitle(tz.name());
        let mut child = self.imp().timezones_listbox.first_child();
        while child.is_some() {
            let c = child.unwrap();
            let check_button: gtk::CheckButton = c.first_child().unwrap().downcast().unwrap();
            if &check_button.label().unwrap() == tz.name() {
                check_button.set_active(true);
            }
            child = c.next_sibling();
        }
    }

    fn search_timezones(&self, search_text: &str) {
        let mut child = self.imp().timezones_listbox.first_child();
        while child.is_some() {
            let c = child.unwrap();
            let check_button: gtk::CheckButton = c.first_child().unwrap().downcast().unwrap();
            if !search_text.is_empty() {
                c.set_visible(check_button.label().unwrap().contains(search_text));
            } else {
                c.set_visible(true);
            }
            child = c.next_sibling();
        }
    }

    fn setup_dateformat_row(&self) {
        let dateformat_comborow = &self.imp().dateformat_comborow;
        let dateformat_model = ListStore::new(crate::store::settings::DateFormats::gobject_type());
        dateformat_comborow.set_model(Some(&dateformat_model));
        dateformat_comborow.set_expression(Some(
            &crate::store::settings::DateFormats::property_expression("name"),
        ));
        for d in crate::store::settings::DateFormats::list() {
            dateformat_model.append(&d.to_gobject());
        }
        dateformat_comborow.connect_selected_notify(|cr| {
            if let Some(selection) = cr.selected_item() {
                let name = selection.property::<String>("name");
                cr.dispatch(crate::store::SET_DATEFORMAT, Some(&name));
            }
        });
    }
}

impl Page for SettingsPage {
    fn name(&self) -> &'static str {
        SETTINGS_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

impl Default for SettingsPage {
    fn default() -> Self {
        Self::new()
    }
}
