use std::{cmp::Ordering, collections::HashMap};

use glib::StaticVariantType;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::adapters::{
    RRULE_FIELD_BYDAY, RRULE_FIELD_COUNT, RRULE_FIELD_INTERVAL, RRULE_FIELD_UNTIL,
    RRULE_VALUE_BYDAY_FRIDAY, RRULE_VALUE_BYDAY_MONDAY, RRULE_VALUE_BYDAY_SATURDAY,
    RRULE_VALUE_BYDAY_SUNDAY, RRULE_VALUE_BYDAY_THURSDAY, RRULE_VALUE_BYDAY_TUESDAY,
    RRULE_VALUE_BYDAY_WEDNESDAY, RRULE_VALUE_DAILY, RRULE_VALUE_MONTHLY, RRULE_VALUE_WEEKLY,
    RRULE_VALUE_YEARLY,
};

use super::{CalendarId, DatetimeFormat, KarlenderDate, KarlenderDateTime};

pub type Repeat = HashMap<String, String>;
pub type EventId = String;

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub enum SyncStatus {
    Synced(String),
    LocallyModified(String),
    LocallyDeleted(String),
    NotSynced,
}

#[derive(Clone, Deserialize, Serialize, PartialEq, Eq)]
#[variant_serde_json]
pub struct Event {
    pub id: EventId,
    pub calendar_url: CalendarId,
    pub item_url: String,
    pub name: String,
    pub location: Option<String>,

    pub start: KarlenderDateTime,
    pub end: KarlenderDateTime,
    pub full_day: bool,

    pub notes: Option<String>,

    pub repeat: Option<Repeat>,

    pub sync_status: SyncStatus,
}

impl Event {
    pub fn new(
        calendar_url: String,
        start: KarlenderDateTime,
        end: KarlenderDateTime,
        full_day: bool,
    ) -> Self {
        let id = Uuid::new_v4().to_string();
        let item_url = format!("{}/{}", calendar_url, id);
        let item_url = item_url.replace("//", "/");
        Event {
            id,
            calendar_url,
            item_url,
            name: "New Event".into(),
            location: None,
            start,
            end,
            full_day,
            notes: None,
            repeat: None,
            sync_status: SyncStatus::NotSynced,
        }
    }
}

impl Event {
    pub fn contains_date(&self, date: &KarlenderDate) -> bool {
        let start_date = self.start.date();
        if date < &start_date {
            return false;
        }

        if self.repeats(RRULE_VALUE_DAILY) {
            let mut interval: i64 = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;
            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL) {
                interval = interv.parse::<i64>().unwrap();
                let delta = date.signed_duration_since(start_date.clone());
                is_in_interval = delta.num_days().rem_euclid(interval) == 0;
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT) {
                let count = count.parse::<i64>().unwrap();
                let delta = date.signed_duration_since(start_date);
                is_before_count_end = (count * interval) - delta.num_days() > 0;
            }
            return is_in_interval && is_before_count_end && is_before_end_date;
        }

        if self.repeats(RRULE_VALUE_WEEKLY) {
            let mut is_weekday = true;
            let mut interval = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;

            if let Some(weekdays) = self.repeat_value(RRULE_FIELD_BYDAY) {
                let weekdays: Vec<&str> = weekdays.split(',').collect();
                is_weekday = match date.weekday().number_from_monday() {
                    0 => weekdays.contains(&RRULE_VALUE_BYDAY_MONDAY),
                    1 => weekdays.contains(&RRULE_VALUE_BYDAY_TUESDAY),
                    2 => weekdays.contains(&RRULE_VALUE_BYDAY_WEDNESDAY),
                    3 => weekdays.contains(&RRULE_VALUE_BYDAY_THURSDAY),
                    4 => weekdays.contains(&RRULE_VALUE_BYDAY_FRIDAY),
                    5 => weekdays.contains(&RRULE_VALUE_BYDAY_SATURDAY),
                    6 => weekdays.contains(&RRULE_VALUE_BYDAY_SUNDAY),
                    _ => false,
                };
            }
            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL) {
                interval = interv.parse::<i64>().unwrap();
                let delta = date.signed_duration_since(start_date.clone());
                let is_after_n_weeks = delta.num_days().rem_euclid(interval * 7) == 0;
                let n_weeks = delta.num_days() / 7;
                is_in_interval = is_after_n_weeks && n_weeks.rem_euclid(interval) == 0;
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT) {
                let count = count.parse::<i64>().unwrap();
                let delta = date.signed_duration_since(start_date);
                is_before_count_end = (count * interval * 7) - delta.num_days() > 0;
            }
            return is_weekday && is_in_interval && is_before_count_end && is_before_end_date;
        }

        if self.repeats(RRULE_VALUE_MONTHLY) {
            let mut interval = 1;
            let mut is_in_interval = true;
            let mut is_before_end_date = true;
            let mut is_before_count_end = true;

            if let Some(interv) = self.repeat_value(RRULE_FIELD_INTERVAL) {
                interval = interv.parse::<i32>().unwrap();
                let event_month = start_date.month() as i32;
                let check_month = date.month() as i32;
                is_in_interval = event_month == check_month
                    || (check_month - event_month).rem_euclid(interval) == 0
            }
            if let Some(end_date) = self.repeat_value(RRULE_FIELD_UNTIL) {
                if let Ok((end_date, _)) = KarlenderDateTime::from_iso8601(end_date, None) {
                    is_before_end_date = date <= &end_date.date();
                }
            }
            if let Some(count) = self.repeat_value(RRULE_FIELD_COUNT) {
                let count = count.parse::<i32>().unwrap();
                let event_year = start_date.year();
                let check_year = date.year();
                let event_month = start_date.month() as i32;
                let check_month = date.month() as i32;
                match event_year.cmp(&check_year) {
                    Ordering::Equal => {
                        is_before_count_end = event_month == check_month
                            || (check_month - event_month) / interval < count;
                    }
                    Ordering::Less => {
                        let months_in_first_year = 12 - event_month;
                        let monhts_of_full_years = (check_year - event_year - 1) * 12;
                        let overall_months =
                            months_in_first_year + monhts_of_full_years + check_month;
                        is_before_count_end = overall_months / interval < count;
                    }
                    Ordering::Greater => {
                        // nothing
                    }
                }
            }
            return is_in_interval
                && is_before_count_end
                && is_before_end_date
                && date.day() == self.start.day();
        }
        if self.repeats(RRULE_VALUE_YEARLY) {
            return date.year() >= self.start.year()
                && date.month() >= self.start.month()
                && date.month() <= self.end.month()
                && date.day() >= self.start.day()
                && date.day() < self.end.day();
        }

        if (date.year() >= self.start.year() && date.year() <= self.end.year())
            && date.month() >= self.start.month()
            && date.month() <= self.end.month()
        {
            if self.start.day() == self.end.day() && date.day() == self.start.day() {
                return true;
            }
            if date.day() >= self.start.day() && date.day() < self.end.day() {
                return true;
            }
        }

        false
    }

    pub fn repeat_value(&self, field: &str) -> Option<&String> {
        self.repeat.as_ref().and_then(|r| r.get(field))
    }

    pub fn repeats(&self, freq: &str) -> bool {
        match self.repeat.as_ref() {
            Some(r) => match r.get("FREQ") {
                Some(v) => v.as_str() == freq,
                None => false,
            },
            None => false,
        }
    }

    pub fn repeat(&self) -> Option<&HashMap<String, String>> {
        self.repeat.as_ref()
    }

    pub fn is_between(&self, start: KarlenderDateTime, end: KarlenderDateTime) -> bool {
        self.start >= start && self.end <= end
    }

    pub fn sort(a: &Self, b: &Self) -> Ordering {
        if a.start == b.start && a.end == b.end {
            a.name.cmp(&b.name)
        } else {
            a.start.cmp(&b.start)
        }
    }
}

impl std::fmt::Debug for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Event")
            .field("id", &self.id)
            .field("calendar_url", &self.calendar_url)
            .field("item_url", &self.item_url)
            .field("name", &self.name)
            .field("location", &self.location)
            .field("start", &self.start.format(DatetimeFormat::ISO8601Z))
            .field("end", &self.start.format(DatetimeFormat::ISO8601Z))
            .field("full_day", &self.full_day)
            .field("notes", &self.notes)
            .field("repeat", &self.repeat)
            .field("sync_status", &self.sync_status)
            .finish()
    }
}

#[cfg(test)]
mod tests {

    use crate::adapters::{
        RRULE_FIELD_COUNT, RRULE_FIELD_FREQ, RRULE_FIELD_INTERVAL, RRULE_VALUE_DAILY,
    };

    use super::*;

    #[test]
    fn test_contains_date_no_repetition() {
        let event = test_event(None);
        assert_eq!(event.contains_date(&date(2022, 4, 3)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 4)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 5)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 6)), false);
    }

    #[test]
    fn test_contains_date_daily_repetition() {
        // daily event ends never
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_DAILY),
            (RRULE_FIELD_INTERVAL, "1"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 3)), false);
        for i in 4..30 {
            assert_eq!(event.contains_date(&date(2022, 4, i)), true);
        }
        for i in 1..32 {
            assert_eq!(event.contains_date(&date(2022, 5, i)), true);
        }
        // daily event ends after 5 times
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_DAILY),
            (RRULE_FIELD_INTERVAL, "1"),
            (RRULE_FIELD_COUNT, "5"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 3)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 4)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 5)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 6)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 7)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 8)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 9)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 10)), false);
        assert_eq!(event.contains_date(&date(2022, 5, 1)), false);
        assert_eq!(event.contains_date(&date(2023, 4, 4)), false);

        // daily event ends after 3 times interval 3
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_DAILY),
            (RRULE_FIELD_INTERVAL, "3"),
            (RRULE_FIELD_COUNT, "3"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 3)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 4)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 5)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 6)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 7)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 8)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 9)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 10)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 12)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 13)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 14)), false);
        assert_eq!(event.contains_date(&date(2022, 5, 1)), false);
        assert_eq!(event.contains_date(&date(2023, 4, 4)), false);

        // daily event ends after 2022.04.10
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_DAILY),
            (RRULE_FIELD_INTERVAL, "1"),
            (RRULE_FIELD_UNTIL, "20220410T000000Z"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 3)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 4)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 5)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 6)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 7)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 8)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 9)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 10)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), false);
        assert_eq!(event.contains_date(&date(2022, 5, 1)), false);
        assert_eq!(event.contains_date(&date(2023, 4, 4)), false);
    }

    #[test]
    fn test_contains_date_weekly_repetition() {
        // weekly event ends never
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_WEEKLY),
            (RRULE_FIELD_INTERVAL, "1"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 12)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 17)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 18)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 19)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 24)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 25)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 26)), false);

        // weekly event ends never interval 2
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_WEEKLY),
            (RRULE_FIELD_INTERVAL, "2"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 12)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 17)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 18)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 19)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 24)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 25)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 26)), false);

        // weekly event ends after 3 times
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_WEEKLY),
            (RRULE_FIELD_INTERVAL, "2"),
            (RRULE_FIELD_COUNT, "3"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 12)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 17)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 18)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 19)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 24)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 25)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 26)), false);
        assert_eq!(event.contains_date(&date(2022, 5, 02)), true);
        assert_eq!(event.contains_date(&date(2022, 5, 09)), false);

        // weekly event ends after date
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_WEEKLY),
            (RRULE_FIELD_INTERVAL, "1"),
            (RRULE_FIELD_UNTIL, "20220417T000000Z"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 4, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 11)), true);
        assert_eq!(event.contains_date(&date(2022, 4, 12)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 17)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 18)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 19)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 24)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 25)), false);
        assert_eq!(event.contains_date(&date(2022, 4, 26)), false);
    }

    #[test]
    fn test_contains_date_monthly_repetition() {
        // monthly event ends never
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_MONTHLY),
            (RRULE_FIELD_INTERVAL, "1"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 04, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 30)), false);
        assert_eq!(event.contains_date(&date(2022, 05, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 06, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 07, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 08, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 09, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 10, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 11, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 12, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 01, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 02, 04)), true);

        // monthly event ends never
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_MONTHLY),
            (RRULE_FIELD_INTERVAL, "2"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 04, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 30)), false);
        assert_eq!(event.contains_date(&date(2022, 05, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 06, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 07, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 08, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 09, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 10, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 11, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 12, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 01, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 02, 04)), true);

        // monthly event ends never interval 2
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_MONTHLY),
            (RRULE_FIELD_INTERVAL, "2"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 04, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 30)), false);
        assert_eq!(event.contains_date(&date(2022, 05, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 06, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 07, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 08, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 09, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 10, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 11, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 12, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 01, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 02, 04)), true);

        // monthly event 6 times interval 2
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_MONTHLY),
            (RRULE_FIELD_INTERVAL, "2"),
            (RRULE_FIELD_COUNT, "6"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 04, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 30)), false);
        assert_eq!(event.contains_date(&date(2022, 05, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 06, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 07, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 08, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 09, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 10, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 11, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 12, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 01, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 02, 04)), true);
        assert_eq!(event.contains_date(&date(2023, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 04, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 05, 04)), false);

        // monthly event until date interval 2
        let event = test_event(Some(vec![
            (RRULE_FIELD_FREQ, RRULE_VALUE_MONTHLY),
            (RRULE_FIELD_INTERVAL, "2"),
            (RRULE_FIELD_UNTIL, "20221013T000000Z"),
        ]));
        assert_eq!(event.contains_date(&date(2022, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 03)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 04, 05)), false);
        assert_eq!(event.contains_date(&date(2022, 04, 30)), false);
        assert_eq!(event.contains_date(&date(2022, 05, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 06, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 07, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 08, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 09, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 10, 04)), true);
        assert_eq!(event.contains_date(&date(2022, 11, 04)), false);
        assert_eq!(event.contains_date(&date(2022, 12, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 01, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 02, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 03, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 04, 04)), false);
        assert_eq!(event.contains_date(&date(2023, 05, 04)), false);
    }

    fn date(year: i32, month: u8, day: u8) -> KarlenderDate {
        KarlenderDate::from_ymd(year, month, day)
    }

    /// 04.04.2022
    fn test_event(repeat: Option<Vec<(&str, &str)>>) -> Event {
        let start = date(2022, 04, 04).and_hms(0, 0, 0, None);
        let end = date(2022, 04, 05).and_hms(0, 0, 0, None);
        let mut e = Event::new("0815".into(), start, end, true);
        e.repeat = repeat.map(|r| {
            r.iter()
                .map(|(k, v)| (k.to_string(), v.to_string()))
                .collect()
        });
        e
    }
}
