use crate::domain::*;
use crate::pages::editor_save;
use crate::store::{
    gstore::{self},
    State,
};
use gettextrs::gettext;
use serde::{Deserialize, Serialize};

use super::ui::InAppNotification;

#[derive(Debug, Default, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct EditorSlice {
    #[serde(skip)]
    pub event: Option<Event>,
}

pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::NEW_EVENT => {
            let mut default_calendar = state.calendar_slice.calendars.values().find(|c| c.default);
            if default_calendar.is_none() {
                default_calendar = state.calendar_slice.calendars.values().next();
            }
            if default_calendar.is_none() {
                state.ui.in_app_notification = Some(InAppNotification {
                    text: gettext("No calendar to create an event for"),
                    action: None,
                });
                return;
            }
            let calendar_url = &default_calendar.unwrap().id;

            if let Some(selection) = &state.selection_slice.selection {
                state.editor.event = Some(Event::new(
                    calendar_url.clone(),
                    selection.start.clone(),
                    selection.end.clone(),
                    selection.full_day,
                ));
                return;
            }

            if let Some(selection) = &state.selection_slice.former_selection {
                state.editor.event = Some(Event::new(
                    calendar_url.clone(),
                    selection.start.clone(),
                    selection.end.clone(),
                    selection.full_day,
                ));
                return;
            }

            let now = KarlenderTimezone::local().now();
            let edit_event = Event::new(calendar_url.clone(), now.clone(), now, true);
            state.editor.event = Some(edit_event);
        }
        crate::store::EDIT_EVENT => {
            let (calendar_id, event_id): (String, String) = action.arg().unwrap();
            if let Some(cal) = state.calendar_slice.calendars.get(&calendar_id) {
                if let Some(event) = cal.events.get(&event_id) {
                    state.editor.event = Some(event.clone());
                }
            }
        }
        crate::store::_INTERNAL_SAVE_EVENT => {
            let event: Event = action.arg().unwrap();
            if let Some(calendar) = state.calendar_slice.calendars.get_mut(&event.calendar_url) {
                if !calendar.is_remote {
                    calendar.events.insert(event.id.clone(), event.clone());
                }
            }
            state.editor.event = None;
        }
        _ => {}
    }
}

#[derive(Default)]
pub struct EditorMiddleware;
impl EditorMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(EditorMiddleware::default())
    }
}

impl gstore::Middleware<crate::store::State> for EditorMiddleware {
    fn pre_reduce(&self, _action: &gstore::Action, _state: &crate::store::State) {}

    fn post_reduce(&self, action: &gstore::Action, _state: &crate::store::State) {
        if action.name() == crate::store::SAVE_EVENT {
            editor_save();
        }
    }
}
