use super::NAVIGATE_BACK;
use crate::{
    pages::{
        ABOUT_PAGE_NAME, ADD_ACCOUNT_PAGE_NAME, DAY_PAGE_NAME, EDITOR_PAGE_NAME, MONTH_PAGE_NAME,
        SETTINGS_PAGE_NAME,
    },
    store::{
        gstore::{self, dispatch},
        store, State,
    },
};
use gettextrs::gettext;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct UI {
    pub current_page: String,
    pub history: Vec<String>,
    pub mobile: bool,
    pub in_app_notification: Option<InAppNotification>,
    pub syncing: bool,
    pub title: String,
    pub dark: bool,
}

impl Default for UI {
    fn default() -> Self {
        Self {
            current_page: "month".into(),
            history: vec!["month".into()],
            mobile: false,
            in_app_notification: None,
            syncing: false,
            title: gettext("Calendar"),
            dark: false,
        }
    }
}

pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::NAVIGATE => {
            let page: String = action.arg().unwrap();
            navigate(page, state);
            update_title(state, None);
        }
        crate::store::_NAVIGATE_WITH_TITLE => {
            let (page, title): (String, String) = action.arg().unwrap();
            navigate(page, state);
            update_title(state, Some(title));
        }
        crate::store::_UPDATE_HISTORY => {
            let page: String = action.arg().unwrap();
            match page.as_str() {
                MONTH_PAGE_NAME => {
                    if state.ui.history.len() == 1 && state.ui.history[0] == MONTH_PAGE_NAME {
                        // do nothing
                    } else {
                        state.ui.history = vec![MONTH_PAGE_NAME.into()];
                    }
                    if state.ui.current_page != MONTH_PAGE_NAME {
                        state.ui.current_page = MONTH_PAGE_NAME.into();
                    }
                }
                _ => {
                    if state.ui.history.last() != Some(&page) {
                        state.ui.history.push(page);
                    }
                }
            }
        }
        crate::store::SYNC => {
            state.ui.syncing = true;
        }
        crate::store::_SYNC_CALENDARS_DONE => {
            state.ui.syncing = false;
        }
        crate::store::_CHANGE_MOBILE => {
            let is_mobile: bool = action.arg().unwrap();
            state.ui.mobile = is_mobile;
        }
        crate::store::_CLEAR_NOTIFICATION => {
            state.ui.in_app_notification = None;
        }
        crate::store::_SHOW_NOTIFICATION => {
            let message: String = action.arg().unwrap();
            state.ui.in_app_notification = Some(InAppNotification {
                text: message,
                action: None,
            })
        }
        _ => {}
    }
}

fn navigate(page: String, state: &mut State) {
    if page == NAVIGATE_BACK {
        let last_element = state.ui.history.len() - 1;

        if last_element == 0 {
            return;
        }

        if let Some(left_page) = state.ui.history.get(last_element) {
            if left_page == "events" {
                state.editor.event = None;
            }
        }

        if let Some(page) = state.ui.history.get(last_element - 1) {
            state.ui.current_page = page.clone();
            state.ui.history.remove(last_element);
        }
    } else {
        state.ui.current_page = page;
    }
}

fn update_title(state: &mut State, specific_title: Option<String>) {
    let page_name = state.ui.current_page.as_str();
    let settings_title = gettext("Settings");
    let event_title = gettext("Event");
    let about_title = gettext("About");
    let login_title = gettext("Login");
    let default_title = gettext("Calendar");
    state.ui.title = match page_name {
        SETTINGS_PAGE_NAME => settings_title,
        EDITOR_PAGE_NAME => event_title,
        ABOUT_PAGE_NAME => about_title,
        ADD_ACCOUNT_PAGE_NAME => login_title,
        MONTH_PAGE_NAME => specific_title.unwrap_or(default_title),
        DAY_PAGE_NAME => {
            if let Some(sel) = state
                .selection_slice
                .selection
                .as_ref()
                .or(state.selection_slice.former_selection.as_ref())
            {
                sel.start
                    .format(crate::domain::DatetimeFormat::DateBottomUp)
            } else {
                default_title
            }
        }
        _ => default_title,
    };
}

#[derive(Default)]
pub struct NotificationMiddleware;
impl NotificationMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(NotificationMiddleware::default())
    }
}
impl gstore::Middleware<State> for NotificationMiddleware {
    fn post_reduce(&self, action: &gstore::Action, state: &State) {
        if action.name() == gstore::INIT {
            if let Some(m) = state.ui.in_app_notification.as_ref() {
                info!("Notification: {:?}", m.text);
                dispatch!(crate::store::_CLEAR_NOTIFICATION)
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InAppNotification {
    pub text: String,
    pub action: Option<(String, String, String)>,
}
