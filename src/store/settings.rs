use crate::{
    domain::{DatetimeFormat, KarlenderTimezone, Login},
    store::*,
};
use serde::{Deserialize, Serialize};
use std::process::Command;

#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Settings {
    pub timezone: Option<String>,
    pub dateformat: Option<DateFormats>,
    #[serde(skip)]
    pub accounts: Vec<Login>,
}

impl Settings {
    pub fn init(state: &mut State) {
        if state.settings.timezone.is_none() {
            let tz = KarlenderTimezone::local();
            state.settings.timezone = Some(tz.name().to_string());
        }
        if state.settings.dateformat.is_none() {
            if let Some(true) = Command::new("gsettings")
                .args(["get", "org.gnome.desktop.interface", "clock-format"])
                .output()
                .ok()
                .and_then(|o| String::from_utf8(o.stdout).ok())
                .map(|s| s.trim().to_string().contains("24"))
            {
                state.settings.dateformat = Some(DateFormats::bottom_down_24());
            } else {
                state.settings.dateformat = Some(DateFormats::bottom_down_12());
            }
        }
    }

    pub fn timezone(&self) -> Option<KarlenderTimezone> {
        self.timezone
            .as_ref()
            .map(|t| KarlenderTimezone::from_str(t))
    }
}

#[gobject(name)]
pub struct Timezone {
    pub name: String,
}

impl From<&KarlenderTimezone> for Timezone {
    fn from(tz: &KarlenderTimezone) -> Self {
        Timezone {
            name: tz.name().to_string(),
        }
    }
}

impl Timezone {
    pub fn list() -> Vec<Timezone> {
        KarlenderTimezone::list()
            .unwrap()
            .iter()
            .map(Timezone::from)
            .collect()
    }
}

static BOTTOM_UP_24: &str = "24h DD.MM.YYYY";
static BOTTOM_UP_12: &str = "12h DD.MM.YYYY";
static TOP_DOWN_24: &str = "24h YYYY.MM.DD";
static TOP_DOWN_12: &str = "12h YYYY.MM.DD";

#[gobject(name)]
#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct DateFormats {
    pub name: String,
    pub datetime: DatetimeFormat,
    pub date: DatetimeFormat,
    pub time: DatetimeFormat,
}

impl DateFormats {
    fn new(
        name: &str,
        datetime: DatetimeFormat,
        date: DatetimeFormat,
        time: DatetimeFormat,
    ) -> Self {
        Self {
            name: name.into(),
            datetime,
            date,
            time,
        }
    }
    pub fn bottom_down_24() -> Self {
        DateFormats::new(
            BOTTOM_UP_24,
            DatetimeFormat::DateBottomUpAndTime24,
            DatetimeFormat::DateBottomUp,
            DatetimeFormat::Time24,
        )
    }

    pub fn bottom_down_12() -> Self {
        DateFormats::new(
            BOTTOM_UP_12,
            DatetimeFormat::DateBottomUpAndTime12,
            DatetimeFormat::DateBottomUp,
            DatetimeFormat::Time12,
        )
    }

    pub fn top_down_24() -> Self {
        DateFormats::new(
            TOP_DOWN_24,
            DatetimeFormat::DateTopDownAndTime24,
            DatetimeFormat::DateTopDown,
            DatetimeFormat::Time24,
        )
    }

    pub fn top_down_12() -> Self {
        DateFormats::new(
            TOP_DOWN_12,
            DatetimeFormat::DateTopDownAndTime12,
            DatetimeFormat::DateTopDown,
            DatetimeFormat::Time12,
        )
    }

    pub fn list() -> [DateFormats; 4] {
        [
            Self::bottom_down_24(),
            Self::bottom_down_12(),
            Self::top_down_24(),
            Self::top_down_12(),
        ]
    }

    fn from_name(name: String) -> Option<Self> {
        if name == BOTTOM_UP_24 {
            Some(Self::bottom_down_24())
        } else if name == BOTTOM_UP_12 {
            Some(Self::bottom_down_12())
        } else if name == TOP_DOWN_24 {
            Some(Self::top_down_24())
        } else if name == TOP_DOWN_12 {
            Some(Self::top_down_12())
        } else {
            None
        }
    }
}

pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::SET_DATEFORMAT => {
            let name: String = action.arg().unwrap();
            state.settings.dateformat = DateFormats::from_name(name)
        }

        crate::store::SELECT_TIMEZONE => {
            let name: String = action.arg().unwrap();
            state.settings.timezone = name.parse().ok();
        }
        crate::store::LOGIN => {
            let (url, login): (String, String) = action.arg().unwrap();
            state.settings.accounts = vec![Login { url, login }];
        }
        crate::store::REMOVE_ACCOUNT => {
            let (_url, _login): (String, String) = action.arg().unwrap();
            state.settings.accounts = vec![];
        }
        _ => {}
    }
}
