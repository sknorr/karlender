use crate::adapters::{Adapter, CaldavAdapter, CaldavAdapterApi};
use crate::domain::*;
use crate::store::{
    gstore::{self, dispatch},
    store, State,
};
use gettextrs::gettext;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::Mutex;

use super::ui::InAppNotification;

#[derive(Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct CalendarsSlice {
    pub calendars: HashMap<CalendarId, Calendar>,
    pub last_sync: Option<KarlenderDateTime>,
    #[serde(skip)]
    pub removed: Vec<(CalendarId, Event)>,
}

impl std::fmt::Debug for CalendarsSlice {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.calendars.values()).finish()
    }
}

const UNDO_REMOVE_EVENT: &str = "undo-remove-event";

pub static SYNCED_CALENDARS: gstore::once_cell::sync::OnceCell<
    Mutex<Option<HashMap<String, Calendar>>>,
> = gstore::once_cell::sync::OnceCell::new();

pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::_ADD_CALENDAR => {
            let new_calendar = Calendar::default();
            state
                .calendar_slice
                .calendars
                .insert(new_calendar.id.clone(), new_calendar);
        }
        crate::store::_REMOVE_CALENDAR => {
            let calendar_id: String = action.arg().unwrap();
            state.calendar_slice.calendars.remove(&calendar_id);
        }
        crate::store::UPDATE_CALENDAR => {
            let (cal_id, props): (String, HashMap<String, glib::Variant>) = action.arg().unwrap();

            let enabled = props
                .get(CALENDAR_PROP_ENABLED)
                .and_then(|v| v.get::<bool>());
            let default = props
                .get(CALENDAR_PROP_DEFAULT)
                .and_then(|v| v.get::<bool>());
            let color = props
                .get(CALENDAR_PROP_COLOR)
                .and_then(|v| v.get::<String>());
            let name = props
                .get(CALENDAR_PROP_NAME)
                .and_then(|v| v.get::<String>());

            if let Some(default) = default {
                if default {
                    for c in &mut state.calendar_slice.calendars.values_mut() {
                        c.default = false;
                    }
                }
            }
            if let Some(name) = name {
                let cal = state.calendar_slice.calendars.get_mut(&cal_id).unwrap();
                if !cal.is_remote {
                    cal.name = name;
                }
            }
            if let Some(cal) = state.calendar_slice.calendars.get_mut(&cal_id) {
                if let Some(default) = default {
                    cal.default = default;
                }
                if let Some(enabled) = enabled {
                    cal.enabled = enabled;
                }
                if let Some(color) = color {
                    cal.color = KarlenderColor::from(color);
                }
            }
        }
        crate::store::_SYNC_CALENDARS_DONE => {
            let error: Option<String> = action.arg().unwrap();
            if let Some(message) = error {
                state.ui.in_app_notification = Some(InAppNotification {
                    text: gettext("Could not sync calendars: {}").replace("{}", &message),
                    action: None,
                });
                return;
            }
            let synced_calendars = SYNCED_CALENDARS.get();
            if let Some(synced_calendars) = synced_calendars {
                let mut synced_calendars = synced_calendars.lock().unwrap();
                state.calendar_slice.calendars = synced_calendars.take().unwrap();
                state.calendar_slice.last_sync = Some(KarlenderTimezone::local().now());
            } else {
                state.ui.in_app_notification = Some(InAppNotification {
                    text: gettext("Could not sync calendars"),
                    action: None,
                });
            }
        }
        crate::store::REMOVE_EVENT => {
            let calendars_slice = &mut state.calendar_slice;
            let (calendar_id, event_id): (String, String) = action.arg().unwrap();

            if let Some(calendar) = calendars_slice.calendars.get_mut(&calendar_id) {
                if let Some(event) = calendar.events.remove(&event_id) {
                    state.ui.in_app_notification = Some(InAppNotification {
                        text: gettext("Removed event '{}'").replace("{}", &event.name),
                        action: Some((
                            gettext("Undo"),
                            format!("app.{}", crate::store::UNDO),
                            UNDO_REMOVE_EVENT.into(),
                        )),
                    });
                    state.calendar_slice.removed.push((calendar_id, event));
                }
            }
        }
        crate::store::UNDO => {
            if let Some(notification) = &state.ui.in_app_notification {
                if let Some((_, _, action_type)) = &notification.action {
                    if action_type == UNDO_REMOVE_EVENT {
                        if let Some((calendar_id, event)) = state.calendar_slice.removed.pop() {
                            state
                                .calendar_slice
                                .calendars
                                .get_mut(&calendar_id)
                                .unwrap()
                                .events
                                .insert(event.id.clone(), event);
                        }
                    }
                }
            }
        }
        crate::store::_CLEAR_NOTIFICATION => {
            if let Some(notification) = &state.ui.in_app_notification {
                if let Some((_, _, action_type)) = &notification.action {
                    if action_type == UNDO_REMOVE_EVENT {
                        state.calendar_slice.removed.pop();
                    }
                }
            }
        }
        _ => {}
    }
}

#[derive(Default)]
pub struct CalDAVSyncMiddleware;
impl CalDAVSyncMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(CalDAVSyncMiddleware::default())
    }
}

impl gstore::Middleware<crate::store::State> for CalDAVSyncMiddleware {
    fn pre_reduce(&self, action: &gstore::Action, state: &crate::store::State) {
        match action.name() {
            crate::store::SYNC => {
                info!("[calendars middleware] SYNC");
                let (sender, receiver) = glib::MainContext::channel::<crate::adapters::caldav::Sync>(
                    glib::PRIORITY_DEFAULT,
                );

                receiver.attach(None, |sync| {
                    match sync {
                        crate::adapters::caldav::Sync::Started => {
                            // nothing to do
                        }
                        crate::adapters::caldav::Sync::Progress => {
                            // nothing to do
                        }
                        crate::adapters::caldav::Sync::Success(calendars) => {
                            let c = SYNCED_CALENDARS.get_or_init(Mutex::default);
                            let mut c = c.lock().unwrap();
                            *c = Some(calendars);
                            drop(c);
                            dispatch!(crate::store::_SYNC_CALENDARS_DONE, None::<String>);
                            return glib::Continue(false);
                        }
                        crate::adapters::caldav::Sync::Failure(message) => {
                            dispatch!(crate::store::_SYNC_CALENDARS_DONE, Some(message));
                            return glib::Continue(false);
                        }
                    }
                    glib::Continue(true)
                });
                CaldavAdapter::sync(sender, &state.calendar_slice.calendars);
            }
            crate::store::_CLEAR_NOTIFICATION => {
                if let Some(notification) = &state.ui.in_app_notification {
                    if let Some((_, _, action_type)) = &notification.action {
                        if action_type == UNDO_REMOVE_EVENT {
                            for (cid, event) in &state.calendar_slice.removed {
                                if let Some(calendar) = state.calendar_slice.calendars.get(cid) {
                                    if calendar.is_remote {
                                        if let Some(event) = calendar.events.get(&event.id) {
                                            CaldavAdapter::remove_event(event);
                                        }
                                        dispatch!(crate::store::SYNC);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }

    fn post_reduce(&self, action: &gstore::Action, state: &crate::store::State) {
        match action.name() {
            crate::store::gstore::INIT => {
                CaldavAdapter::init();
                if let Ok((login, password)) = CaldavAdapter::get_login() {
                    // Do not send the password around. It must not be logged.
                    let mutex = LOGIN_PASSWORD.get_or_init(Default::default);
                    let mut lock = mutex.lock().unwrap();
                    *lock = Some(password);
                    drop(lock);

                    dispatch!(crate::store::LOGIN, (login.url, login.login));
                }
            }

            crate::store::LOGIN => {
                let (url, login): (String, String) = action.arg().unwrap();
                if let Some(mutex) = LOGIN_PASSWORD.get() {
                    if let Ok(mut lock) = mutex.lock() {
                        if let Some(password) = lock.take() {
                            CaldavAdapter::login(url, login, password);
                        }
                    }
                }
            }

            crate::store::REMOVE_ACCOUNT => {
                let (_url, _login): (String, String) = action.arg().unwrap();
                CaldavAdapter::remove_login();
            }

            crate::store::_INTERNAL_SAVE_EVENT => {
                let event: Event = action.arg().unwrap();
                if let Some(calendar) = state.calendar_slice.calendars.get(&event.calendar_url) {
                    if calendar.is_remote {
                        if let Some(created_event) = CaldavAdapter::save_event(event) {
                            debug!("Updated event in remote: {:?}", created_event);
                            dispatch!(crate::store::SYNC);
                        }
                    }
                } else {
                    error!("Could not find calendar for event {:?}", event);
                }
            }
            _ => {}
        }
    }
}

pub static LOGIN_PASSWORD: gstore::once_cell::sync::OnceCell<Mutex<Option<String>>> =
    gstore::once_cell::sync::OnceCell::new();
