use crate::store::{gstore, State};
use glib::once_cell::sync::OnceCell;
use gstore::Action;
use std::{collections::HashSet, fs::create_dir_all, io::Write};

pub const OLD_APP_DIR: &str = ".karlender";
pub const APP_DIR: &str = "karlender";
pub const STATE_FILE_NAME: &str = "state.json";

static IGNORED_EVENTS: OnceCell<HashSet<&'static str>> = OnceCell::new();

pub fn save(action: &str, state: &State) {
    let ignores = IGNORED_EVENTS.get_or_init(|| {
        let mut hs = HashSet::new();
        hs.insert(crate::store::_UPDATE_HISTORY);
        hs.insert(crate::store::_CHANGE_MOBILE);
        hs.insert(crate::store::_CLEAR_NOTIFICATION);
        hs.insert(crate::store::_LOAD_LOCAL_CALENDARS);
        hs.insert(crate::store::_GO_NEXT);
        hs.insert(crate::store::_GO_PREV);
        hs.insert(crate::store::NAVIGATE);
        hs.insert(crate::store::_NAVIGATE_WITH_TITLE);
        hs.insert(crate::store::NEW_EVENT);
        hs.insert(crate::store::SELECT);
        hs.insert(crate::store::EDIT_EVENT);
        hs.insert(crate::store::LOGIN);
        hs.insert(crate::store::SAVE_EVENT);
        hs
    });

    if ignores.contains(&action) {
        debug!("Skip state persistence: No changes");
        return;
    }

    if let Some(error) = &state.error {
        error!("Did not save state: {}", error);
        return;
    }

    match serde_json::to_vec(&state) {
        Ok(buf) => {
            let mut app_dir = glib::user_data_dir();
            app_dir.push(APP_DIR);

            if !app_dir.exists() {
                if let Err(e) = create_dir_all(&app_dir) {
                    error!("Could not create app dir: {}", e);
                    return;
                }
            }

            debug!("Save state in {:?}", app_dir);
            let mut state_file = std::fs::File::create(app_dir.join(STATE_FILE_NAME))
                .expect("Could not create app state file.");
            if let Err(e) = state_file.write_all(&buf) {
                error!("Could not save app state: {}", e);
            }
        }
        Err(e) => error!("Serialize state: {}", e),
    }
}

pub fn load() -> Result<State, String> {
    let old_app_dir = get_old_app_dir();
    let app_dir = get_app_dir();
    if old_app_dir.exists() {
        if let Err(e) = std::fs::rename(old_app_dir, &app_dir) {
            error!("{}", e);
            panic!("Failed to migrate old app dir: {}", e);
        }
    }
    if !app_dir.exists() {
        if let Err(e) = create_dir_all(&app_dir) {
            panic!("Could not create app dir: {}", e);
        }
    }
    debug!("Load state from {:?}", app_dir);
    let path = app_dir.join(STATE_FILE_NAME);

    if !path.exists() {
        let prefix = path.parent().unwrap();
        create_dir_all(prefix).unwrap();
        let file = std::fs::File::create(&path).expect("Could not initialize state file.");
        serde_json::to_writer(file, &State::default()).expect("Could ");
    }

    match crate::migration::migrate(&path) {
        Ok(buf) => match serde_json::from_slice(&buf) {
            Ok(state) => {
                debug!("Loaded state.");
                Ok(state)
            }
            Err(e) => Err(format!("Could not migrate local data: {}", e)),
        },
        Err(e) => Err(format!("Could not migrate local data: {}", e)),
    }
}

pub fn get_old_app_dir() -> std::path::PathBuf {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(OLD_APP_DIR);
    app_dir
}

pub fn get_app_dir() -> std::path::PathBuf {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(APP_DIR);
    app_dir
}

pub struct LocalPersistenceMiddleware;
impl LocalPersistenceMiddleware {
    pub fn new() -> Box<Self> {
        Box::new(LocalPersistenceMiddleware)
    }
}
impl gstore::Middleware<crate::store::State> for LocalPersistenceMiddleware {
    fn post_reduce(&self, action: &Action, state: &crate::store::State) {
        if crate::store::store().is_last_reduce_phase() && action.name() != gstore::INIT {
            save(action.name(), state)
        }
    }
}
