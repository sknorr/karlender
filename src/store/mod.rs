use self::persistence::load;
pub use gtk_rust_app::gstore;
use serde::{Deserialize, Serialize};

pub mod calendars;
pub mod event_editing;
pub mod persistence;
pub mod selection;
pub mod settings;
pub mod ui;

pub const _REMOVE_CALENDAR: &str = ".remove-calendar";
pub const _ADD_CALENDAR: &str = ".add-calendar";
pub const _INTERNAL_SAVE_EVENT: &str = ".internal-save-event";
pub const _UPDATE_HISTORY: &str = ".update-history";
pub const _CHANGE_MOBILE: &str = ".change-mobile";
pub const _SHOW_NOTIFICATION: &str = ".show-notification";
pub const _CLEAR_NOTIFICATION: &str = ".clear-notification";
pub const _LOAD_LOCAL_CALENDARS: &str = ".load_local_calendars";
pub const _SYNC_CALENDARS_DONE: &str = ".sync-calendars-done";
pub const _GO_PREV: &str = ".go-prev";
pub const _GO_NEXT: &str = ".go-next";
pub const _NAVIGATE_WITH_TITLE: &str = ".navigate-with-title";

include!("../../target/gra-gen/actions.rs");

pub const NAVIGATE_BACK: &str = "back";

#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct State {
    #[serde(skip)]
    pub error: Option<String>,
    #[serde(skip)]
    pub ui: ui::UI,
    #[serde(skip)]
    pub selection_slice: selection::SelectionSlice,
    #[serde(skip)]
    pub editor: event_editing::EditorSlice,

    pub calendar_slice: calendars::CalendarsSlice,
    pub settings: settings::Settings,
    pub version: String,
}

impl State {
    pub fn init() -> Self {
        let mut state = match load() {
            Ok(state) => state,
            Err(error) => State::error(error),
        };
        settings::Settings::init(&mut state);
        state
    }

    pub fn error(error: String) -> Self {
        Self {
            error: Some(error),
            ..Default::default()
        }
    }
}

gstore::store!(State);

pub fn reduce(action: &gstore::Action, state: &mut State) {
    let start = std::time::Instant::now();
    trace!("Pre: {:?}", state);
    info!("Reduce: {}", action);
    calendars::reduce(action, state);
    ui::reduce(action, state);
    event_editing::reduce(action, state);
    selection::reduce(action, state);
    settings::reduce(action, state);
    debug!(
        "Reduced: {:?} {}ms",
        action,
        std::time::Instant::now().duration_since(start).as_millis()
    );
    trace!(
        "Post: {:?} {}ms",
        state,
        std::time::Instant::now().duration_since(start).as_millis()
    );
}
