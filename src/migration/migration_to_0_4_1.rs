use semver::VersionReq;
use serde_json::Value;

const VERSION_REQ: &str = "<0.4.1";

pub fn migrate(json: &mut Value) {
    let version = super::get_version(json);
    let version_check = VersionReq::parse(VERSION_REQ).unwrap();
    debug!(
        "Check migration for version {} with {}",
        version,
        &VERSION_REQ[1..]
    );
    if version_check.matches(&version) {
        let new_version = VERSION_REQ[1..].into();
        debug!("Migrate from \"{}\" to {}", version, new_version);
        json["version"] = new_version;
        migrate_calendars(json);
    }
}

fn migrate_calendars(json: &mut Value) {
    if let Some(calendars) = json["calendar_slice"]["calendars"].as_object_mut() {
        for (_cid, calendar) in calendars {
            migrate_event(calendar);
        }
    }
}

fn migrate_event(calendar: &mut Value) {
    if let Some(events) = calendar["events"].as_object_mut() {
        for (_eid, event) in events {
            if let Some(event) = event.as_object_mut() {
                migrate_repeat(event);
            }
        }
    }
}

fn migrate_repeat(event: &mut serde_json::Map<String, Value>) {
    if let Some(repeat) = event.get("repeat").and_then(|r| r.as_array()) {
        let mut repeat_map = serde_json::Map::new();
        for repeat_kv_pair in repeat {
            if let Some(repeat_array) = repeat_kv_pair.as_array() {
                if let Some(key) = repeat_array[0].as_str() {
                    let value = &repeat_array[1];
                    repeat_map.insert(key.to_string(), value.clone());
                }
            }
        }
        event["repeat"] = Value::Object(repeat_map);
    }
}
