use semver::{Version, VersionReq};
use serde_json::Value;

const VERSION_REQ: &str = "<0.4.0";

pub fn migrate(json: &mut Value) {
    let version = super::get_version(json);
    let version_check = VersionReq::parse(VERSION_REQ).unwrap();
    debug!(
        "Check migration for version {} with {}",
        version,
        &VERSION_REQ[1..]
    );
    if version_check.matches(&version) {
        let new_version = VERSION_REQ[1..].into();
        if version == Version::parse("0.3.17").unwrap() {
            json["version"] = new_version;
            return;
        }
        debug!("Migrate from \"{}\" to {}", version, new_version);
        json["version"] = new_version;
        if let Some(calendars) = json["calendar_slice"]["calendars"].as_object_mut() {
            for (_cid, calendar) in calendars {
                calendar["is_remote"] = true.into();
            }
        }
    }
}
