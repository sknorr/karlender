use std::fs::File;
use std::io::Read;
use std::path::Path;

use semver::Version;
use serde_json::Value;

mod migration_to_0_4_0;
mod migration_to_0_4_1;

pub fn migrate(state_file_path: &Path) -> Result<Vec<u8>, MigrationError> {
    let mut state_file = File::open(state_file_path)?;
    let mut data = Vec::new();
    state_file.read_to_end(&mut data)?;
    let mut json = serde_json::from_slice::<Value>(&data[..])?;

    // run all migration sets.
    // The order must never be changed.
    // migration sets must never be changed.
    migration_to_0_4_0::migrate(&mut json);
    migration_to_0_4_1::migrate(&mut json);

    Ok(serde_json::to_vec(&json)?)
}

fn get_version(json: &Value) -> Version {
    json["version"]
        .as_str()
        .and_then(|s| Version::parse(s).ok())
        .unwrap_or_else(|| Version::parse("0.3.16").unwrap())
}

#[derive(Debug)]
pub struct MigrationError(String);

impl std::fmt::Display for MigrationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<std::io::Error> for MigrationError {
    fn from(e: std::io::Error) -> Self {
        MigrationError(e.to_string())
    }
}

impl From<serde_json::Error> for MigrationError {
    fn from(e: serde_json::Error) -> Self {
        MigrationError(e.to_string())
    }
}

#[cfg(test)]
mod tests {
    use crate::store::State;

    use super::*;

    pub fn state_mock() -> Value {
        serde_json::from_str(include_str!("state_0.3.16.json")).unwrap()
    }

    pub fn expected_state() -> Value {
        serde_json::from_str(include_str!("expected_state.json")).unwrap()
    }

    #[test]
    fn test_migrate() {
        let mut json = state_mock();

        migration_to_0_4_0::migrate(&mut json);

        assert_eq!(json["version"].as_str(), Some("0.4.0"));
        assert_eq!(
            json["calendar_slice"]["calendars"]["https://dav.example.com/1234/"]["is_remote"]
                .as_bool(),
            Some(true)
        );
    }

    #[test]
    fn test_migrate_not_twice() {
        let mut json = state_mock();
        json["version"] = "0.3.17".into();

        migration_to_0_4_0::migrate(&mut json);

        assert_eq!(json["version"].as_str(), Some("0.4.0"));
        assert_eq!(
            json["calendar_slice"]["calendars"]["https://dav.example.com/1234/"]["is_remote"]
                .as_bool(),
            None
        );
    }

    #[test]
    fn test_migrate_all() {
        let mut state = state_mock();

        migration_to_0_4_0::migrate(&mut state);
        migration_to_0_4_1::migrate(&mut state);

        let expected_state = expected_state();
        assert!(
            state == expected_state,
            "Actual State: \n\n{}\n\n",
            serde_json::to_string(&state).unwrap()
        );
        let parsed_state: State = serde_json::from_value(state).unwrap();
        assert_eq!(parsed_state.version, "0.4.1");
    }
}
