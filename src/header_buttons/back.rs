use crate::store::{gstore::select, store, State, NAVIGATE_BACK};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "back.ui")]
pub struct BackHeaderButton {
    #[template_child]
    pub button: TemplateChild<gtk::Button>,
}

impl BackHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create BackHeaderButton")
    }
    pub fn constructed(&self) {
        let s = self;
        self.imp().button.set_action_name(Some("app.navigate"));
        self.imp().button.set_action_target(Some(&NAVIGATE_BACK));
        select!(|state| state.ui => glib::clone!(@weak s => move |state| {
            let ui = &state.ui;
            s.set_visible(ui.history.len() > 1);
        }));
    }
}

impl Default for BackHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
