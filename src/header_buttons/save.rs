use crate::store::{gstore::*, store, State, NAVIGATE_BACK};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "save.ui")]
pub struct SaveHeaderButton {
    #[template_child]
    pub button: TemplateChild<gtk::Button>,
}

impl SaveHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create SaveHeaderButton")
    }
    pub fn constructed(&self) {
        let s = self;
        let button = &self.imp().button;

        select!(|state| state.ui => glib::clone!(@weak s => move |state| {
            let mobile = state.ui.mobile;
            let page = &state.ui.current_page;
            s.set_visible(!mobile && page == crate::pages::EDITOR_PAGE_NAME);
        }));

        button.connect_clicked(|_b| {
            dispatch!(crate::store::SAVE_EVENT);
            dispatch!(crate::store::NAVIGATE, NAVIGATE_BACK)
        });
    }
}

impl Default for SaveHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
