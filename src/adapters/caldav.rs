use super::Adapter;
use crate::domain::*;
use gettextrs::gettext;
use glib::once_cell::sync::OnceCell;
use secret_service::SecretService;
use std::collections::HashMap;
use url::Url;

pub const RRULE_FIELD_FREQ: &str = "FREQ";
pub const RRULE_VALUE_YEARLY: &str = "YEARLY";
pub const RRULE_VALUE_MONTHLY: &str = "MONTHLY";
pub const RRULE_VALUE_WEEKLY: &str = "WEEKLY";
pub const RRULE_VALUE_DAILY: &str = "DAILY";
pub const RRULE_VALUE_HOURLY: &str = "HOURLY";

pub const RRULE_FIELD_BYDAY: &str = "BYDAY";
pub const RRULE_VALUE_BYDAY_MONDAY: &str = "MO";
pub const RRULE_VALUE_BYDAY_TUESDAY: &str = "TU";
pub const RRULE_VALUE_BYDAY_WEDNESDAY: &str = "WE";
pub const RRULE_VALUE_BYDAY_THURSDAY: &str = "TH";
pub const RRULE_VALUE_BYDAY_FRIDAY: &str = "FR";
pub const RRULE_VALUE_BYDAY_SATURDAY: &str = "SA";
pub const RRULE_VALUE_BYDAY_SUNDAY: &str = "SU";

pub const RRULE_FIELD_INTERVAL: &str = "INTERVAL";

pub const RRULE_FIELD_COUNT: &str = "COUNT";
pub const RRULE_FIELD_UNTIL: &str = "UNTIL";
pub trait CaldavAdapterApi: Adapter {
    fn login(url: String, login: String, password: Password) -> Option<Login>;
    fn get_login() -> Result<(Login, Password), &'static str>;
    fn remove_login();
    fn sync(sender: glib::Sender<Sync>, calendars: &HashMap<CalendarId, Calendar>);
    fn remove_event(event: &Event);
    fn save_event(event: Event) -> Option<Event>;
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum Sync {
    Started,
    Progress,
    Success(HashMap<CalendarId, Calendar>),
    Failure(String),
}

static SECRET_SERVICE: OnceCell<Result<SecretService, secret_service::Error>> = OnceCell::new();

pub struct CaldavAdapter;
impl Adapter for CaldavAdapter {
    fn init() {}
}
impl CaldavAdapterApi for CaldavAdapter {
    fn login(url: String, login: String, password: Password) -> Option<Login> {
        Self::remove_login();

        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return None;
        }
        let service = service.as_ref().unwrap();
        if let Ok(c) = service.get_default_collection() {
            let mut attributes = HashMap::new();
            attributes.insert("label", "karlender");
            attributes.insert("url", &url);
            attributes.insert("login", &login);

            if let Err(e) = c.create_item(
                "karlender",
                attributes,
                password.as_bytes(),
                true,
                "Password",
            ) {
                error!("Could not store secret: {:?}", e);
            } else {
                return Some(Login { url, login });
            }
        }
        None
    }

    fn remove_login() {
        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return;
        }
        if let Ok(service) = service.as_ref() {
            if let Ok(c) = service.get_default_collection() {
                let mut attributes = HashMap::new();
                attributes.insert("label", "karlender");
                if let Ok(items) = c.search_items(attributes) {
                    for item in items {
                        item.delete().unwrap();
                    }
                }
            }
        } else {
            error!("Could not remove login: Can not talk to secret service API.")
        }
    }

    fn get_login() -> Result<(Login, Password), &'static str> {
        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return Err("Could not access secret-service API");
        }
        if let Ok(service) = service.as_ref() {
            if let Ok(c) = service.get_default_collection() {
                let mut attributes = HashMap::new();
                attributes.insert("label", "karlender");
                if let Ok(items) = c.search_items(attributes) {
                    if let Some(item) = items.first() {
                        if let Ok(attr) = item.get_attributes() {
                            if let Some(url) = attr.get(&"url".to_string()) {
                                if let Some(email) = attr.get(&"login".to_string()) {
                                    if let Some(password) = item
                                        .get_secret()
                                        .ok()
                                        .and_then(|s| String::from_utf8(s).ok())
                                    {
                                        return Ok((
                                            Login {
                                                url: url.clone(),
                                                login: email.clone(),
                                            },
                                            password,
                                        ));
                                    } else {
                                        return Err("No password found in karlender entry in secret storage");
                                    }
                                } else {
                                    return Err("No 'email' field found in karlender entry in secret store.");
                                }
                            } else {
                                return Err(
                                    "No 'url' field found in karlender entry in secret store.",
                                );
                            }
                        } else {
                            return Err("No karlender entry found in secret store");
                        }
                    }
                }
            }
        } else {
            return Err("Invalid internal state");
        }
        Err("No login found")
    }

    fn sync(sender: glib::Sender<Sync>, calendars: &HashMap<CalendarId, Calendar>) {
        debug!("Start syncing");
        let mut merged_calendars = calendars.clone();
        std::thread::spawn(move || {
            sender
                .send(Sync::Started)
                .expect("Faild to send sync update");

            match Self::get_login() {
                Ok((login, password)) => {
                    let agent = ureq::Agent::new();
                    if let Ok(url) = Url::parse(&login.url) {
                        let mut err = None;
                        match minicaldav::get_calendars(
                            agent.clone(),
                            &login.login,
                            &password,
                            &url,
                        ) {
                            Ok(cals) => {
                                debug!("Got calendars: {:?}", cals);
                                let mut synced_calendars = HashMap::new();
                                for cal in cals {
                                    let calendar_url = cal.url().as_str().to_string();
                                    let name = cal.name().clone();
                                    let mut events = HashMap::new();
                                    match minicaldav::get_events(
                                        agent.clone(),
                                        &login.login,
                                        &password,
                                        &cal,
                                    ) {
                                        Ok((evs, errors)) => {
                                            for ev in evs {
                                                let url = ev.url().as_str().to_string();
                                                match Event::try_from((calendar_url.clone(), ev)) {
                                                    Ok(event) => {
                                                        events.insert(url, event);
                                                    }
                                                    Err(e) => {
                                                        error!("Syncing event: {}", e)
                                                    }
                                                }
                                            }
                                            for er in errors {
                                                error!("{:?}", er);
                                                err = Some(er);
                                            }
                                        }
                                        Err(e) => {
                                            error!("{:?}", e);
                                            err = Some(e);
                                        }
                                    };
                                    debug!(
                                        "Synced {} events for calendar {}.",
                                        events.len(),
                                        cal.name()
                                    );
                                    synced_calendars.insert(
                                        calendar_url.clone(),
                                        Calendar {
                                            id: calendar_url,
                                            name,
                                            events,
                                            is_remote: true,
                                            ..Default::default()
                                        },
                                    );
                                    sender
                                        .send(Sync::Progress)
                                        .expect("Faild to send sync update");
                                }

                                Self::merge(&synced_calendars, &mut merged_calendars);

                                sender
                                    .send(Sync::Success(merged_calendars))
                                    .expect("Failed to send sync update");
                            }
                            Err(e) => {
                                error!("{:?}", e);
                                err = Some(e);
                            }
                        }

                        if let Some(e) = err {
                            send_error(e, &sender);
                        }
                    } else {
                        Self::remove_login();
                        sender
                            .send(Sync::Failure("Invalid URL in you secret store".to_string()))
                            .expect("Failed to send sync update");
                    }
                }
                Err(msg) => {
                    sender
                        .send(Sync::Failure(gettext("No CalDAV login found!")))
                        .expect("Failed to send sync update");
                    error!("Login not found: {}", msg)
                }
            }
        });
    }

    fn remove_event(event: &Event) {
        if let Ok((login, password)) = Self::get_login() {
            let agent = ureq::Agent::new();
            match minicaldav::Event::try_from(event) {
                Ok(event) => {
                    if let Err(e) = minicaldav::remove_event(agent, &login.login, &password, event)
                    {
                        error!("CalDAV Error: {:?}", e);
                    }
                }
                Err(e) => error!("Remove event: {}", e),
            }
        }
    }

    fn save_event(event: Event) -> Option<Event> {
        if let Ok((login, password)) = Self::get_login() {
            let agent = ureq::Agent::new();
            match minicaldav::Event::try_from(&event) {
                Ok(event) => {
                    if let Err(e) = minicaldav::save_event(agent, &login.login, &password, event) {
                        error!("CalDAV Error: {:?}", e);
                    }
                }
                Err(e) => error!("Saving event: {}", e),
            }

            Some(event)
        } else {
            None
        }
    }
}

fn send_error(e: minicaldav::Error, sender: &glib::Sender<Sync>) {
    match e {
        minicaldav::Error::Ical(_) => {
            sender
                .send(Sync::Failure(gettext("Invalid ICAL Event format.")))
                .ok();
        }
        minicaldav::Error::Caldav(_) => {
            sender
                .send(Sync::Failure(gettext("Failure in CalDAV communication.")))
                .ok();
        }
    }
}

impl CaldavAdapter {
    fn merge(
        synced_calendars: &HashMap<String, Calendar>,
        merged_calendars: &mut HashMap<String, Calendar>,
    ) {
        for (synced_calendar_id, synced_calendar) in synced_calendars {
            merged_calendars.insert(synced_calendar_id.clone(), synced_calendar.clone());
        }
    }
}

impl TryFrom<&Event> for minicaldav::Event {
    type Error = String;

    fn try_from(event: &Event) -> Result<Self, Self::Error> {
        let etag = match &event.sync_status {
            SyncStatus::Synced(etag) => Some(etag),
            SyncStatus::LocallyModified(etag) => Some(etag),
            SyncStatus::LocallyDeleted(etag) => Some(etag),
            SyncStatus::NotSynced => None,
        };

        let repeat: Option<Vec<(String, String)>> = if event.repeat.is_some() {
            let freq = event
                .repeat()
                .and_then(|r| r.get(RRULE_FIELD_FREQ).cloned())
                .unwrap_or_else(|| "".into());

            if freq == RRULE_VALUE_WEEKLY {
                event.repeat.as_ref().map(|repeat| {
                    repeat
                        .iter()
                        .map(|(k, v)| (k.clone(), v.clone()))
                        .filter(|(k, v)| {
                            k == RRULE_FIELD_BYDAY && !v.is_empty()
                                || k == RRULE_FIELD_FREQ
                                || k == RRULE_FIELD_COUNT
                                || k == RRULE_FIELD_UNTIL
                                || k == RRULE_FIELD_INTERVAL
                        })
                        .collect()
                })
            } else {
                Some(
                    event
                        .repeat
                        .as_ref()
                        .unwrap()
                        .iter()
                        .map(|(k, v)| (k.clone(), v.clone()))
                        .filter(|(k, _)| {
                            k == RRULE_FIELD_FREQ
                                || k == RRULE_FIELD_COUNT
                                || k == RRULE_FIELD_UNTIL
                                || k == RRULE_FIELD_INTERVAL
                        })
                        .collect(),
                )
            }
        } else {
            None
        };

        let rrule: Option<String> = repeat.map(|r| {
            r.into_iter()
                .map(|(k, v)| format!("{}={}", k, v))
                .collect::<Vec<String>>()
                .join(";")
        });

        let mut date_attributes = vec![];
        let start;
        let end;
        if event.full_day {
            date_attributes.push(("VALUE", "DATE"));
            start = event.start.format(DatetimeFormat::ISO8601DATE);
            end = event.end.format(DatetimeFormat::ISO8601DATE);
        } else {
            start = event.start.format(DatetimeFormat::ISO8601Z);
            end = event.end.format(DatetimeFormat::ISO8601Z);
        }
        let tz = KarlenderTimezone::local();

        if let Ok(url) = Url::parse(&event.item_url) {
            let builder = minicaldav::Event::builder(url)
                .etag(etag.cloned())
                .uid(event.id.clone())
                .summary(event.name.clone())
                .location(event.location.clone())
                .description(event.notes.clone())
                .start(start, date_attributes.clone())
                .end(end, date_attributes)
                .rrule(rrule)
                .timestamp(tz.to_utc(&tz.now()).format(DatetimeFormat::ISO8601Z));

            let event = builder.build();
            debug!("Created ICAL Event: {:#?}", event);
            Ok(event)
        } else {
            Err(format!("Url '{}' of event is not valid.", event.item_url))
        }
    }
}

impl TryFrom<(String, minicaldav::Event)> for Event {
    type Error = String;

    fn try_from((calendar_id, event): (String, minicaldav::Event)) -> Result<Self, Self::Error> {
        let mut tz = None;
        if let Some(start) = event.property("DTSTART") {
            let end = event.property("DTEND").unwrap();
            if let Some(tz_name) = end.attribute("TZID") {
                let tz_name = tz_name.replace('-', "/");
                tz = Some(KarlenderTimezone::from_str(&tz_name));
            }
            let start_date_string: String = start.value().clone();
            let end_date_string: String = if let Some(end) = event.property("DTEND") {
                if tz.is_none() {
                    if let Some(tz_name) = end.attribute("TZID") {
                        let tz_name = tz_name.replace('-', "/");
                        tz = Some(KarlenderTimezone::from_str(&tz_name));
                    }
                }
                end.value().clone()
            } else {
                start.value().clone()
            };
            match KarlenderDateTime::from_iso8601(&start_date_string, tz.as_ref()) {
                Ok((start, full_day)) => {
                    match KarlenderDateTime::from_iso8601(&end_date_string, tz.as_ref()) {
                        Ok((end, _)) => {
                            let name = event
                                .property("SUMMARY")
                                .map(|p| p.value().clone())
                                .unwrap_or_else(|| "Unnamed".to_string());

                            let start = if let Some(tz) = &tz {
                                tz.to_utc(&start)
                            } else {
                                start
                            };

                            let end = if let Some(tz) = &tz {
                                tz.to_utc(&end)
                            } else {
                                end
                            };

                            Ok(Self {
                                id: event.url().as_str().to_string(),
                                calendar_url: calendar_id,
                                item_url: event.url().as_str().to_string(),
                                name,
                                location: event.get("LOCATION").cloned(),
                                start,
                                end,
                                full_day,
                                notes: event.property("DESCRIPTION").map(|p| p.value().clone()),
                                repeat: event.property("RRULE").map(|p| p.value().clone()).map(
                                    |d| {
                                        d.split(';')
                                            .filter_map(|kv| kv.split_once('='))
                                            .map(|(k, v)| (k.to_string(), v.to_string()))
                                            .collect()
                                    },
                                ),
                                sync_status: SyncStatus::Synced(event.etag().unwrap().clone()),
                            })
                        }
                        Err(e) => Err(format!("Could not parse event date: {:?}", e)),
                    }
                }
                Err(e) => Err(format!(
                    "Could not parse event date '{}': {:?}",
                    start.value(),
                    e
                )),
            }
        } else {
            Err(format!(
                "Could not parse event '{}': No start date",
                event.url()
            ))
        }
    }
}
