VERSION=$1
DATE=$(git log -1 --date=short --format=%ad v$VERSION)
RELEASE_CONTENT=$(./ci/generate-flatpak-changelog.sh "v$VERSION")
TEMPLATE=$(echo "  # this_comment_is_required_for_automated_release\n\
    { version = \"${VERSION}\", date = \"${DATE}\", description = \"\"\"\n\
$RELEASE_CONTENT
\
\"\"\" },")

sed -i "s/  # this_comment_is_required_for_automated_release/$TEMPLATE/g" Cargo.toml