# Karlender development artifact

## Prerequisites

When using this artifact you need to install some dependencies first:

```
sudo apt install flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install org.gnome.Sdk//42 -y
sudo flatpak install org.gnome.Platform//42 -y
sudo flatpak install org.freedesktop.Sdk.Extension.rust-stable//21.08 -y
```

Now you can install Karlender via

```
sudo flatpak install ./karlender.flatpak
```

Run karlender via

```
flatpak run codes.loers.Karlender
```