# karlender

[![pipeline status](https://gitlab.com/loers/karlender/badges/main/pipeline.svg)](https://gitlab.com/loers/karlender/-/commits/main)

<!-- ![patrons](https://img.shields.io/liberapay/patrons/loers.svg?logo=liberapay") -->

GTK calendar application written in Rust.

Karlender is a mobile-friendly calendar app. It uses libadwaita and can access Calendars using CalDAV.

<img src="screenshots/portrait.png" width="200">

## Support

Wether you submit issues or contribute code: I appreciate all help.

<!-- If you like Karlender you can support me via librapay:

<a href="https://liberapay.com/loers/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a> -->

## Installation/ Usage

Karlender is a flatpak app and available on [flathub](https://flathub.org/apps/details/codes.loers.Karlender).

Otherwise you can run Karleder and set the log level via:

```
RUST_LOG=error,info,debug,trace cargo run
```

## License

Karlender is distributed under the terms of the GPL-3.0 license. See LICENSE for details.

## Project status

In active development.

## Further Documentation

- [roadmap](ROADMAP.md)
- [contribute](CONTRIBUTE.md)
